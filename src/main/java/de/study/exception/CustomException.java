/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.study.exception;

/**
 *
 * @author PLemos
 */
public class CustomException extends Exception {

    public CustomException() {
    }


    public static class CustomExceptionSon extends CustomException {

        public CustomExceptionSon() {
        }
    }
    
    public static class CustomExceptionGrandSon extends CustomExceptionSon {
        public CustomExceptionGrandSon() {
        }
    }

    public static CustomExceptionSon createCustomSom(){
        return new CustomExceptionSon();
    }
    
    public static CustomExceptionGrandSon createCustomExceptionGrandSon(){
        return new CustomExceptionGrandSon();
    }

}
