/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.study.exception;

/**
 *
 * @author PLemos
 */
public class CloseableInstance implements AutoCloseable {

    private final StringBuilder sb;
    
    private final Exception exception;
    
    public CloseableInstance(final StringBuilder sb, Exception exception) {
        this.sb = sb;
        this.exception =  exception;
    }
    
    @Override
    public void close() throws Exception {
        sb.append("Within the close method");
        if (exception != null) {
            sb.append("Throw inside close method");
            throw exception;
        }
    }
    
}
