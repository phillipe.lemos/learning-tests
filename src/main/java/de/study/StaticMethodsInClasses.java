/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study;

/** @author PLemos */
public class StaticMethodsInClasses {

	static class Animal {
		public static String testStaticMethod(){
      return "Animal.testStaticMethod";
		}
	}
	
	static class AnimalDescendent extends Animal {
		public static String testStaticMethod(){
      return "AnimalDescendent.testStaticMethod";
		}
	}
	
	public static void main(String...a) {
		Animal animalDescendent = new AnimalDescendent();
		animalDescendent.testStaticMethod();
		AnimalDescendent animalDescendent1 = new AnimalDescendent();
		animalDescendent1.testStaticMethod();
	}
	
}
