/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study.interfaces;

/** @author PLemos */
interface A{
	default void method_a(){
		System.out.println("method_a - from A");
	}
	
	static void method_b(){
		System.out.println("method_b - from A");
	}
}

interface SonOfA extends A {
	@Override
		void method_a();
	
	void method_b(); // here I am hidding the method_b from the parent
}

interface GrandSonOfA extends SonOfA {
	@Override
	default void method_a(){
				System.out.println("method_a - from GrandSonOfA");
	}
	
	@Override
	default void method_b(){
		System.out.println("method_b - from GrandSonOfA");
	}
}

public class SomeInterestingInterfaces {}
