/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study.classlifetime;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This is a Quiz by
 * https://blogs.oracle.com/javamagazine/java-quiz-garbage-collector-gc?source=:em:nw:mt::::RC_WWMK200429P00043C0029:NSL400161332
 * 
 * Even after the instance of Demo is removed from memory, the objects created inside the doIt method could be found
 * by the static List l. This could lead a memory leak if many calls of doIt is done.
 * 
 */
public class Demo {

  public static ArrayList<Object> l = new ArrayList<>();

  public void doIt() {
    HashMap<String, Object> m = new HashMap<>();
    Object o1 = new Object();
    Object o2 = new Object();
    m.put("o1", o1);
    o1 = o2;
    o1 = null;
    l.add(m);
    m = null;
    System.gc();
  }
}
