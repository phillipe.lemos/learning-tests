/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study.functionalPrograming;

import java.util.Optional;
import java.util.Properties;
import java.util.function.BiFunction;
import java.util.function.Supplier;

/**
 * https://blogs.oracle.com/javamagazine/java-quiz-optional-null-exception?source=:em:nw:mt::::RC_WWMK200429P00043C0029:NSL400161332
 */
public class Config extends Properties {
  static class ConfigException extends RuntimeException {
    ConfigException(String message) {
      super(message);
    }
  }

  public String getProperty(final String key) {
    String property = super.getProperty(key);
    if (property == null) {
      throw new ConfigException(String.format("Missing value for key '%s'!", key));
    }
    return property;
  }

  public String getPropertyWithoutException(final String key) {
    return super.getProperty(key);
  }

  private BiFunction<Boolean, String, Optional<String>> compose =
      (flag, val) -> {
        return flag ? Optional.of(val) : Optional.empty();
      };

  public Optional<String> getConfigString(String key) {
    return compose.apply(this.containsKey(key), this.getProperty(key));
  }

	/**
	 * When flag variable is false, the Supplier.get isn't not executed, so we do not have 
	 * the ConfigException thrown.
	 */
  private BiFunction<Boolean, Supplier<String>, Optional<String>> composeWithSupplier =
      (flag, val) -> {
        return flag ? Optional.of(val.get()) : Optional.empty();
      };

	/**
	 * By passing the supplier the call of getProperty is deferred until Supplier.get is executed.
	 * @param key
	 * @return 
	 */
  public Optional<String> getConfigStringUsingSupplier(String key) {
    return composeWithSupplier.apply(this.containsKey(key), () -> this.getProperty(key));
  }
}
