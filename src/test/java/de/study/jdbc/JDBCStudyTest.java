/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.study.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.management.RuntimeErrorException;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.equalTo;
import org.junit.AfterClass;
import static org.junit.Assert.fail;
import org.junit.BeforeClass;
import org.junit.Test;
import de.study.ConsumerThrowable;

/**
 *
 * @author PLemos
 */
public class JDBCStudyTest {

    private final static String URL = "jdbc:derby:memory:zoo;create=true";

    private final static String ERROR_MESSAGE = "This invocation should result in an SQLException wrapped into a RuntimeException";

    @BeforeClass
    public static void setUpClass() throws SQLException {
        try (Connection con = DriverManager.getConnection(URL);
                Statement smt = con.createStatement()) {
            smt.executeUpdate("create table animals ( id integer primary key, name varchar(255))");
            smt.executeUpdate("insert into animals values ( 1, 'Animal 1')");
            smt.executeUpdate("insert into animals values ( 2, 'Animal 2')");
            smt.executeUpdate("insert into animals values ( 3, 'Animal 3')");
            smt.executeUpdate("insert into animals values ( 4, 'Animal 4')");
            smt.executeUpdate("insert into animals values ( 5, 'Animal 5')");
        }
    }

    @AfterClass()
    public static void tearDownClass() throws SQLException {
        try (Connection con = DriverManager.getConnection(URL + ";shutdown=true")) {

        } catch (SQLException ex) {
            if (!ex.getSQLState().equals("08006")) {
                throw ex;
            }
        }
    }

    @Test
    public void iteratenOverTheResultSet() {
        executeQueryAndPostAction("select count(*) from animals", ResultSet.TYPE_FORWARD_ONLY, rs -> {
            if (rs.next()) {
                assertThat(rs.getInt(1), equalTo(5));
            }
        });
    }

    @Test(expected = RuntimeException.class)
    public void accessingPreviousMethodInForwasOnlyCursor() {
        executeQueryAndPostAction("select count(*) from animals", ResultSet.TYPE_FORWARD_ONLY, rs -> rs.previous());
        fail("This method invocation should cause an exception");
    }

    @Test(expected = RuntimeException.class)
    public void tryToGetDataFromEmptyDataSet() {
        executeQueryAndPostAction("select * from animals where id = -999", ResultSet.TYPE_FORWARD_ONLY, rs -> {
            rs.next();
            rs.getInt(1);
        });
        fail(ERROR_MESSAGE);
    }

    @Test(expected = RuntimeException.class)
    public void tryToGetResultBeforePointingToAValidPosition() throws Exception {
        executeQueryAndPostAction("select * from animals", ResultSet.TYPE_FORWARD_ONLY, rs -> rs.getInt(1));
        fail(ERROR_MESSAGE);
    }

    @Test(expected = RuntimeException.class)
    public void tryingToAccessIndexZeroInResultSet() {
        executeQueryAndPostAction("select * from animals", ResultSet.TYPE_FORWARD_ONLY, rs -> rs.getInt(0));
        fail(ERROR_MESSAGE);
    }

    @Test(expected = RuntimeException.class)
    public void tryingToAccesNonExistingColumnByName() {
        executeQueryAndPostAction("select * from animals", ResultSet.TYPE_FORWARD_ONLY, rs -> rs.getInt("XXXX"));
        fail(ERROR_MESSAGE);
    }

    @Test(expected = RuntimeException.class)
    public void getRecorsBeforeFirst() {
        executeQueryAndPostAction("select * from animals", ResultSet.TYPE_SCROLL_INSENSITIVE, rs -> {
            rs.beforeFirst();
            rs.getInt(1);
        });
        fail(ERROR_MESSAGE);
    }
    
    @Test(expected = RuntimeException.class)
    public void getRecorsAfterLast() {
        executeQueryAndPostAction("select * from animals", ResultSet.TYPE_SCROLL_INSENSITIVE, rs -> {
            rs.afterLast();
            rs.getInt(1);
        });
        fail(ERROR_MESSAGE);
    }

    @Test
    public void usingScrollingResultSet() {
        executeQueryAndPostAction("select * from animals", ResultSet.TYPE_SCROLL_INSENSITIVE, rs -> {
            assertThat(rs.first(), equalTo(Boolean.TRUE));
            assertThat(rs.last(), equalTo(Boolean.TRUE));
            assertThat(rs.getInt(1), equalTo(rs.getInt("id")));
            int id = rs.getInt(1);
            while (rs.previous()) {
                id--;
                assertThat(id, equalTo(rs.getInt(1)));
            }
            id = 1;
            while(rs.absolute(id)){
                assertThat(id, equalTo(rs.getInt(1)));
                ++id;
            }
            rs.last();
            id = rs.getInt(1);
            id--;
            while(rs.relative(-1)){
                assertThat(id, equalTo(rs.getInt(1)));
                id--;
            }
        });
    }

    private void executeQueryAndPostAction(String sql, int cursorType, ConsumerThrowable<ResultSet> customerConsumer) {
        try (Connection con = DriverManager.getConnection(URL);
                Statement smt = con.createStatement(cursorType, ResultSet.CONCUR_READ_ONLY);
                ResultSet rs = smt.executeQuery(sql);) {
            customerConsumer.accept(rs);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Test
    public void statementExecuteReturnsResultSet() throws SQLException {
        try (Connection con = DriverManager.getConnection(URL);
                Statement smt = con.createStatement();) {
            boolean isResultSet = smt.execute("select * from animals");
            if (isResultSet) {
                ResultSet resultSet = smt.getResultSet();
                try {
                    while (resultSet.next()) {
                        assertThat(resultSet.getInt(1), greaterThan(0));
                    }
                } finally {
                    resultSet.close();
                }
            } else {
                fail("This invocation should result in an SQLException");
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Test
    public void statementExecuteReturnsLinesAffected() throws SQLException {
        try (Connection con = DriverManager.getConnection(URL);
                Statement smt = con.createStatement();) {
            boolean isResultSet = smt.execute("insert into animals values ( 6, 'Animal 6')");
            if (!isResultSet) {
                int rowsAffected = smt.getUpdateCount();
                assertThat(rowsAffected, equalTo(1));
            } else {
                fail("This invocation should result in an SQLException");
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
