package de.study;

public class OSHelper {

	public static boolean IS_WINDOWS_OS() {
		return System.getProperty("os.name").toLowerCase().startsWith("windows");
	}

}
