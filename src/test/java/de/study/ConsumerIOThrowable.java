/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study;

import java.io.IOException;

/** *  @author PLemos
 * @param <U> 
 */
@FunctionalInterface
public interface ConsumerIOThrowable<U> extends ConsumerThrowable<U> {
  
	@Override
	void accept(U u) throws IOException;
	
	
}
