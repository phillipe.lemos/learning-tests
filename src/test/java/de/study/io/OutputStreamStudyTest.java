/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study.io;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import org.junit.Test;

/** @author PLemos */
public class OutputStreamStudyTest {

	@Test
	public void checkingWriteMethod() throws IOException {
		byte[] buffer = "Test output stream".getBytes();
		try(OutputStream ou = new	ByteArrayOutputStream()) {
      ou.write(buffer[0]);
			assertThat(((ByteArrayOutputStream)ou).size(),  greaterThan(0) );
		}
		
		try(OutputStream ou = new	ByteArrayOutputStream()) {
			ou.write(buffer);
			assertThat(((ByteArrayOutputStream)ou).size(),  equalTo(buffer.length));
		}
		
		try(OutputStream ou = new	ByteArrayOutputStream()) {
			ou.write(buffer, 0, buffer.length);
			assertThat(((ByteArrayOutputStream)ou).size(),  equalTo(buffer.length));
		}
		
	}

}
