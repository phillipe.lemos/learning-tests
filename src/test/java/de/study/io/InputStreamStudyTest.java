/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study.io;

import static de.study.Message.FAIL_MESSAGE;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.fail;
import org.junit.Test;

/** @author PLemos */
public class InputStreamStudyTest {

	
	private final File realFile = new File("src/test/resources/test.txt");
	
  
	@Test	
	public void tryingOpenAInexistentFile()  {
		try {
			 new FileInputStream(new File("src/test/resources/test.abc"));
			 fail("This point could not be reach due the expected FileNotFoundException");
		}catch(FileNotFoundException f){}
		try{
			 new FileInputStream(new File("src/test/resources"));
			 fail(FAIL_MESSAGE);
		} catch(FileNotFoundException f){}
		File file = null;
		try{
			 new FileInputStream(file);
			 fail("This point could not be reach due the expected FileNotFoundException");
    } catch (NullPointerException | FileNotFoundException f) {
    }
	}
	
	/**
	 * The mark method is supported by BufferedStreams, ByteArrayInputStream. 
	 * FilterInputStream has a constructor protected and could not be access by an external class.
	 * @throws IOException 
	 */
	@Test
  public void checkMarkSupportMethod() throws IOException  {
		try(InputStream io = new FileInputStream(realFile);){
			assertThat(io.markSupported(), equalTo(Boolean.FALSE));
		}
		
		try(InputStream bs = new BufferedInputStream(new FileInputStream(realFile));){
			assertThat(bs.markSupported(), equalTo(Boolean.TRUE));
		}

    try (InputStream bs = new ByteArrayInputStream("my byte test".getBytes());) {
			assertThat(bs.markSupported(), equalTo(Boolean.TRUE));
		}
	}
	
	@Test
	public void checkingMarkAndResetMethods() throws IOException  {
		int itemRead = -1;
		int itemRead2 = -1; 
		try(InputStream bs = new BufferedInputStream(new FileInputStream(realFile));){
			bs.mark(100);
			itemRead = bs.read();
      bs.reset();
			itemRead2 = bs.read();
			assertThat( itemRead, not(equalTo(-1)));
			assertThat( itemRead2, not(equalTo(-1)));
			assertThat( itemRead, equalTo(itemRead2) );
		}
	}
	
	@Test
	public void checkingAvailableMethod() throws IOException {
		try(InputStream bs = new BufferedInputStream(new FileInputStream(realFile));){
      assertThat(bs.available(), greaterThan(0));
		}
		
		try(InputStream bs = new FileInputStream(realFile);){
      assertThat(bs.available(), greaterThan(0));
		}
	}
	
	@Test
	public void checkingReadMethod() throws IOException {
		try(InputStream bs = new BufferedInputStream(new FileInputStream(realFile));){
			assertThat( bs.read(), not(equalTo(-1)));
		}
		byte[] buffer = new byte[1024];
		int numberOfBytesRead = -1;
		try(InputStream bs = new BufferedInputStream(new FileInputStream(realFile));){
			numberOfBytesRead = bs.read(buffer);
			assertThat(numberOfBytesRead, not(equalTo(-1)));
			for(int i = 0; i < numberOfBytesRead; i++) {
				assertThat((int)buffer[i],  greaterThan(0) );
			}
		}
		
		try(InputStream bs = new BufferedInputStream(new FileInputStream(realFile));){
			numberOfBytesRead = bs.read(buffer, 0, buffer.length);
			assertThat(numberOfBytesRead, not(equalTo(-1)));
			for(int i = 0; i < numberOfBytesRead; i++) {
				assertThat((int)buffer[i],  greaterThan(0) );
			}
		}
	}
	
	@Test
	public void checkingReadAllBytesMethod() throws IOException {
		try(InputStream bs = new BufferedInputStream(new FileInputStream(realFile));){
			byte[] buffer = bs.readAllBytes();
			assertThat(buffer, notNullValue());
			assertThat(buffer.length, greaterThan(1));
			for(int i = 0; i < buffer.length; i++){
				assertThat((int)buffer[i],  greaterThan(0) );
			}
		}
	}

  @Test
  public void checkingTransferToMethod() throws IOException {
		try(InputStream bs = new BufferedInputStream(new FileInputStream(realFile));
				ByteArrayOutputStream bout = new ByteArrayOutputStream();){
			bs.transferTo(bout);
      byte[] buffer = bout.toByteArray();
			assertThat(buffer, notNullValue());
			assertThat(buffer.length, greaterThan(1));
			for(int i = 0; i < buffer.length; i++){
				assertThat((int)buffer[i],  greaterThan(0) );
			}
		}
		
		try(InputStream bs = new BufferedInputStream(new FileInputStream(realFile));){
			bs.transferTo(null);
			fail("It is expected a NullPointerException");
		}catch(Exception n) {
			assertThat( n, instanceOf( NullPointerException.class ));
		}
		
	}
}

