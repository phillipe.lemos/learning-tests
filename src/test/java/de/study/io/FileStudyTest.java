/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.study.io;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.nio.file.Paths;
import java.sql.Date;
import java.time.LocalDate;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;

/**
 * @author PLemos
 */
public class FileStudyTest {

    private static final String USER_DIR = System.getProperty("user.dir");

    private static final String MOCK_DIR = "/parent-folder/some-child-folder";

    private File realFile;

    private File mockFile;

    @Before
    public void setUp() {
        realFile = buildFileByName(USER_DIR);
        mockFile = buildFileByName(MOCK_DIR);
    }

    private File buildFileByName(final String fileName) {
        return new File(fileName);
    }

    @Test
    public void verifyingFileExistence() {
        assertThat(realFile.exists(), equalTo(Boolean.TRUE));
        assertThat(mockFile.exists(), equalTo(Boolean.FALSE));
    }

    /**
     * The method getName does enforce the existence of the path represented by
     * the File instance. So even with an inexistent path in filesystem, this
     * method does not throw any exception
     */
    @Test
    public void checkingGetNameMethod() {
        assertThat(realFile.getName(), equalTo("learning-tests"));
        assertThat(mockFile.getName(), equalTo("some-child-folder"));
    }

    /**
     * The method getAbsolutePath does enforce the existence of the path
     * represented by the File instance. So even with an inexistent path in
     * filesystem, this method does not throw any exception
     */
    @Test
    public void checkingAbslutePathMethod() {
        assertThat(realFile.getAbsolutePath(), equalTo(USER_DIR));
        for (String segment : MOCK_DIR.split("/")) {
            assertThat(mockFile.getAbsolutePath().contains(segment), equalTo(Boolean.TRUE));
        }
    }

    @Test
    public void checkingIsDirectory() {
        assertThat(realFile.isDirectory(), equalTo(Boolean.TRUE));
        assertThat(mockFile.isDirectory(), equalTo(Boolean.FALSE)); // the result is
        // expected due the
        // inexistence of the
        // path
    }

    @Test
    public void checkingIsFile() {
        assertThat(realFile.isFile(), equalTo(Boolean.FALSE));
        assertThat(mockFile.isFile(), equalTo(Boolean.FALSE)); // the result is expected
        // due the inexistence of
        // the path
        assertThat(buildFileByName("src/test/resources/test.txt").isFile(), equalTo(Boolean.TRUE));
    }

    @Test
    public void checkingLength() {
        // for directories the length method not garantee zero as returning value.
        // assertThat(realFile.length(), equalTo(Long.valueOf(0)));
        // for inexistents paths it is garantee the the return value will be zero.
        assertThat(mockFile.length(), equalTo(Long.valueOf(0)));
        assertThat(buildFileByName("src/test/resources/test.txt").length(), greaterThan(Long.valueOf(0)));
    }

    @Test
    public void checkingLastModifed() {
        long lastModified = buildFileByName("src/test/resources/test.txt").lastModified();
        assertThat(lastModified, greaterThan(Long.valueOf(0)));
        // The assuption here is that the test.txt won't be changed.
        LocalDate date = (new Date(lastModified)).toLocalDate();
        assertThat(mockFile.lastModified(), equalTo(Long.valueOf(0)));
    }

    @Test
    public void checkingDelete() {
        File directory = buildFileByName("src/test/resources");
        assertThat(directory.delete(), equalTo(Boolean.FALSE));
        assertThat(mockFile.delete(), equalTo(Boolean.FALSE));
        File subDirectory = buildFileByName("src/test/resources/a");
        assertThat(subDirectory.exists(), equalTo(Boolean.FALSE));
        assertThat(subDirectory.mkdir(), equalTo(Boolean.TRUE));
        assertThat(subDirectory.exists(), equalTo(Boolean.TRUE));
        assertThat(subDirectory.delete(), equalTo(Boolean.TRUE));
    }

    @Test
    public void checkingRenameTo() {
        File subDirectory = buildFileByName("src/test/resources/a");
        assertThat(subDirectory.exists(), equalTo(Boolean.FALSE));
        assertThat(subDirectory.mkdir(), equalTo(Boolean.TRUE));
        assertThat(subDirectory.exists(), equalTo(Boolean.TRUE));
        File dest = buildFileByName("src/test/resources/b");
        assertThat(subDirectory.renameTo(dest), equalTo(Boolean.TRUE));
        assertThat(dest.exists(), equalTo(Boolean.TRUE));
        assertThat(dest.delete(), equalTo(Boolean.TRUE));
        try {
            subDirectory.renameTo(null);
            fail("Should not reach this point due an expected NullpointerException ");
        } catch (NullPointerException e) {
        }
    }

    /**
     * Here I will only test the false return. The return true is already tests
     * in checkRenameTo.
     */
    @Test
    public void checkingMrdir() {
        assertThat(mockFile.mkdir(), equalTo(Boolean.FALSE));
    }

    @Test
    public void checkingMkdirs() {
        File subDirectory = buildFileByName("src/test/resources/a/b/c/d");
        assertThat(subDirectory.mkdirs(), equalTo(Boolean.TRUE));
        assertThat(subDirectory.exists(), equalTo(Boolean.TRUE));
        while (!subDirectory.getName().endsWith("resources")) {
            assertThat(subDirectory.delete(), equalTo(Boolean.TRUE));
            subDirectory = subDirectory.getParentFile();
        }
        subDirectory = buildFileByName("src/test/resources/a");
        assertThat(subDirectory.exists(), equalTo(Boolean.FALSE));
        // If there is a root folder,
        // assertThat( mockFile.mkdirs(), equalTo(Boolean.FALSE) );
    }

    @Test
    public void checkingParent() {
        File dir = buildFileByName("src/test/resources");
				File expectedParent = new File("src/test");
				File expectedMore = new File("/parent-folder");
        assertThat(dir.getParent().equals(expectedParent.toString()), equalTo(Boolean.TRUE));
        assertThat(mockFile.getParent().equals(expectedMore.toString()), equalTo(Boolean.TRUE));
    }

    @Test
    public void checkListFiles() {
        File dir = buildFileByName("src/test/resources");
        File[] files = dir.listFiles();
        FileFilter filter = null;
        FilenameFilter filenameFilter = null;
				
        assertListFiles(files, "test.txt", 1);
        assertThat(mockFile.listFiles(), nullValue());
        assertListFiles(dir.listFiles(filter), "test.txt", 1);
        assertThat(mockFile.listFiles(filter), nullValue());

        assertListFiles(dir.listFiles(filePath -> filePath.equals(new File("src/test/resources/test.txt"))), "test.txt",
                1);
        assertListFiles(dir.listFiles(filePath -> filePath.equals(new File("src/test/resources/test.abc"))), null, 0);
        assertListFiles(dir.listFiles(filePath -> filePath.equals(new File("src/test/resources"))), null, 0);
        assertListFiles(dir.listFiles(filenameFilter), "test.txt", 1);
        assertListFiles(dir.listFiles((directory, fileName) -> Stream.of(directory.listFiles())
                .anyMatch(f -> f.getName().equals(fileName) && fileName.endsWith("test.txt"))), "test.txt", 1);

        assertListFiles(dir.listFiles((directory, fileName) -> Stream.of(directory.listFiles())
                .anyMatch(f -> f.getName().equals(fileName) && fileName.endsWith("test.abc"))), null, 0);

    }

    private void assertListFiles(File[] files, String fileName, int length) {
        assertThat(files.length, equalTo(length));
        if (fileName != null) {
            assertThat(files[0].getName(), equalTo(fileName));
        }
    }

}
