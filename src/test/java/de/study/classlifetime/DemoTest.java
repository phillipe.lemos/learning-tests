/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study.classlifetime;

import java.util.Map;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.nullValue;
import org.junit.Test;

/** @author PLemos */
public class DemoTest {
	

	@Test
	public void objectsCouldBeFoundEventAfterDemoRefIsNull(){
		Demo demo = new Demo();
		demo.doIt();
		demo = null;
		assertThat(demo, nullValue());
    assertThat(Demo.l.get(0), instanceOf(Map.class));
		Map map = (Map)Demo.l.get(0);
		assertThat(map.get("o1"), instanceOf(Object.class));
	}
	
}
