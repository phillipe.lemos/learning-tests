/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study;

/** @author PLemos */
@FunctionalInterface
public interface BiConsumerThrowable<T, U> {
	
	void accept(T t, U u) throws Exception;
	
}
	


