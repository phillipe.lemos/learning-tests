/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.study.nio;

import de.study.OSHelper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.NoSuchFileException;
import java.nio.file.NotDirectoryException;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.nio.file.attribute.UserPrincipal;
import java.util.List;
import java.util.stream.Stream;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import org.junit.Test;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.fail;
import org.junit.Before;
import de.study.ConsumerIOThrowable;
import de.study.ConsumerThrowable;

/**
 * @author PLemos Methods from Files that should be tested.
 *     <ul>
 *       <li>copy - OK
 *       <li>createDirectories - Ok
 *       <li>createFile - OK
 *       <li>createLink
 *       <li>createSymbolicLInk
 *       <li>createTempDirectory
 *       <li>createTempFile
 *       <li>delete - OK
 *       <li>deleteIfExists - OK
 *       <li>exists - OK
 *       <li>find
 *       <li>getAttribute
 *       <li>getFileAttributeView
 *       <li>getFileStore
 *       <li>getLastModifiedTime - OK
 *       <li>getOwner - OK
 *       <li>getPosixFilePermissions
 *       <li>isDirectory​ OK
 *       <li>isExecutable - OK
 *       <li>isHidden - OK
 *       <li>isReadable - OK
 *       <li>isRegularFile​
 *       <li>isSameFile
 *       <li>isSymbolicLink
 *       <li>isWritable
 *       <li>lines
 *       <li>list
 *       <li>move - OK
 *       <li>newBufferedReader - OK
 *       <li>newBufferedWriter - OK
 *       <li>newByteChannel
 *       <li>newDirectoryStream
 *       <li>newInputStream
 *       <li>newOutputStream
 *       <li>notExists
 *       <li>probeContentType
 *       <li>readAllBytes
 *       <li>readAllBytes
 *       <li>readAllLines - OK
 *       <li>readAttributes - OK
 *     </ul>
 */
public class FilesStudyTest {

  private Path sourcePath;

  private Path directoryAPath;

  private Path mockInexistentFile;

  private LinkOption[] options;

  @Before
  public void setUp() {
    sourcePath = Paths.get("src/test/resources/test.txt");
    directoryAPath = Paths.get("src/test/resources/a");
    mockInexistentFile = Paths.get("src/test/resources/test1.txt");
  }

  @Test
  public void isDirectorySuccess() {
    Path path = Paths.get(System.getProperty("java.home"));
    boolean expected = Files.isDirectory(path, LinkOption.NOFOLLOW_LINKS);
    assertThat(expected, equalTo(Boolean.TRUE));
    assertThat(
        Files.isDirectory(Paths.get("/a/b.txt"), LinkOption.NOFOLLOW_LINKS),
        equalTo(Boolean.FALSE));
  }

  @Test
  public void checkingCreateFileMethod() throws IOException {
    Path filePath = Paths.get("src/test/resources/a.txt");
    assertThat(filePath, equalTo(Files.createFile(filePath)));
    assertThat(filePath.toFile().delete(), equalTo(Boolean.TRUE));
    Files.createFile(filePath);
    treatExceptionWhenCreateFileOrDirectory(
        FileAlreadyExistsException.class, p -> Files.createFile(p), filePath);
    treatExceptionWhenCreateFileOrDirectory(
        NullPointerException.class, p -> Files.createFile(p), null);
    assertThat(filePath.toFile().delete(), equalTo(Boolean.TRUE));
  }

  @Test
  public void checkingCreateDirectoriesMethod() throws IOException {
    Path filePath = Paths.get("src/test/resources/a/b");
    assertThat(filePath.toAbsolutePath(), equalTo(Files.createDirectories(filePath)));
    File file = filePath.toAbsolutePath().toFile();
    assertThat(file.isDirectory(), equalTo(Boolean.TRUE));
    assertThat(file.delete(), equalTo(Boolean.TRUE));
    assertThat(file.getParentFile().exists(), equalTo(Boolean.TRUE));
    assertThat(file.getParentFile().delete(), equalTo(Boolean.TRUE));
    Files.createFile(directoryAPath);
    treatExceptionWhenCreateFileOrDirectory(
        FileAlreadyExistsException.class, p -> Files.createDirectories(p), directoryAPath);

    treatExceptionWhenCreateFileOrDirectory(
        NullPointerException.class, p -> Files.createDirectories(p), null);
    assertThat(Paths.get("src/test/resources/a").toFile().delete(), equalTo(Boolean.TRUE));
  }

  private void treatExceptionWhenCreateFileOrDirectory(
      Class expectedException, ConsumerThrowable<Path> comamnd, Path filePath) {
    try {
      comamnd.accept(filePath);
      fail("Should not reach this line due a expected exception in the previous line");
    } catch (Exception ex) {
      assertThat(ex.getClass(), equalTo(expectedException));
    }
  }

  @Test
  public void investigatingCopyUsingOutputStreamMethod() throws Exception {
    File expectedFile = mockInexistentFile.toFile();
    try (OutputStream fos = new FileOutputStream(expectedFile)) {
      Files.copy(sourcePath, fos);
      assertThat(expectedFile.exists(), equalTo(Boolean.TRUE));
    }
    // This assertion need to be out of the try-with-resources, otherwise
    // it will not wotk due the file handle.
    assertThat(expectedFile.delete(), equalTo(Boolean.TRUE));

    copyWithOutputFailures(null, sourcePath);
    // Here the copy will not be done, instead we will have a zero-file size.
    copyWithOutputFailures(new FileOutputStream(expectedFile), null);
    assertThat(expectedFile.exists(), equalTo(Boolean.TRUE));
    assertThat(expectedFile.delete(), equalTo(Boolean.TRUE));
    copyWithOutputFailures(null, null);
  }

  private void copyWithOutputFailures(OutputStream fos, Path sourcePath) {
    try {
      Files.copy(sourcePath, fos);
      fail("It is not supposed to processed this path!");
    } catch (IOException | NullPointerException ex) {
      assertThat(ex, instanceOf(NullPointerException.class));
    } finally {
      if (fos != null) {
        try {
          fos.close();
        } catch (IOException io) {
          throw new RuntimeException(io);
        }
      }
    }
  }

  @Test
  public void investigatingCopyUsingInputStream() throws Exception {
    Path expectedPath = mockInexistentFile;
    File sourceFile = sourcePath.toFile();
    try (InputStream in = new FileInputStream(sourceFile)) {
      Files.copy(in, expectedPath);
      File expectedFile = expectedPath.toFile();
      assertThat(expectedFile.exists(), equalTo(Boolean.TRUE));
      // As the try-with-resources referes to the inputstream, the delete assert could be inside the
      // try block
      assertThat(expectedFile.delete(), equalTo(Boolean.TRUE));
    }
    copyWithInputStreamFailures(null, expectedPath);
    copyWithInputStreamFailures(new FileInputStream(sourceFile), null);
    copyWithInputStreamFailures(null, null);
  }

  private void copyWithInputStreamFailures(InputStream in, Path expectedPath) {
    try {
      Files.copy(in, expectedPath);
    } catch (IOException | NullPointerException ex) {
      assertThat(ex, instanceOf(NullPointerException.class));
    } finally {
      if (in != null) {
        try {
          in.close();
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
      }
    }
  }

  @Test
  public void investigatingCopyUsingOnlyPaths() throws Exception {
    // Path sourcePath = Paths.get("src/test/resources/test.txt");
    Path expectedPath = mockInexistentFile;
    File fileExpected = expectedPath.toFile();
    Files.copy(sourcePath, expectedPath);
    assertThat(fileExpected.exists(), equalTo(Boolean.TRUE));
    assertThat(fileExpected.delete(), equalTo(Boolean.TRUE));
    Files.copy(sourcePath, expectedPath);
    try {
      Files.copy(sourcePath, expectedPath);
      fail("It is expected that the previous command generate an exception");
    } catch (IOException ex) {
      assertThat(ex, instanceOf(FileAlreadyExistsException.class));
    }
    Files.copy(
        sourcePath, expectedPath, new StandardCopyOption[] {StandardCopyOption.REPLACE_EXISTING});
    assertThat(fileExpected.exists(), equalTo(Boolean.TRUE));
    assertThat(fileExpected.delete(), equalTo(Boolean.TRUE));
  }

  @Test
  public void investigatingExistsMethod() {
    assertThat(Files.exists(Paths.get("src/test/resources/test.txt")), equalTo(Boolean.TRUE));
    assertThat(Files.exists(mockInexistentFile), equalTo(Boolean.FALSE));
    assertThat(Files.exists(Paths.get("/a/b")), equalTo(Boolean.FALSE));
  }

  @Test
  public void investigatingMoveMethod() throws Exception {
    Path sourceDirPath = directoryAPath;
    Path destinationPath = Paths.get("src/test/resources/b");
    File sourceFile = sourceDirPath.toFile();
    File destinationFile = destinationPath.toFile();
    Files.createDirectory(sourceDirPath);
    assertThat(sourceFile.exists(), equalTo(Boolean.TRUE));
    assertThat(sourceFile.isDirectory(), equalTo(Boolean.TRUE));
    Files.move(sourceDirPath, destinationPath);
    assertThat(destinationFile.exists(), equalTo(Boolean.TRUE));
    assertThat(destinationFile.isDirectory(), equalTo(Boolean.TRUE));
    assertThat(sourceFile.exists(), equalTo(Boolean.FALSE));
    assertThat(destinationFile.delete(), equalTo(Boolean.TRUE));
  }

  @Test
  public void investigatingDeleteMethod() throws Exception {
    Path sourceDirPath = directoryAPath;
    Path resultPath = Files.createDirectories(sourceDirPath);
    assertThat(resultPath, equalTo(sourceDirPath));
    assertThat(resultPath.toFile().exists(), equalTo(Boolean.TRUE));
    Files.delete(resultPath);
    assertThat(resultPath.toFile().exists(), equalTo(Boolean.FALSE));
    resultPath = Files.createDirectories(sourceDirPath);
    assertThat(resultPath.toFile().exists(), equalTo(Boolean.TRUE));
    assertThat(Files.deleteIfExists(resultPath), equalTo(Boolean.TRUE));
    assertThat(Files.deleteIfExists(resultPath), equalTo(Boolean.FALSE));
  }

  @Test
  public void investigationDeleteFailures() {
    try {
      Files.delete(directoryAPath);
      fail("It is expected that the previous command generate an exception");
    } catch (IOException ex) {
      assertThat(ex, instanceOf(NoSuchFileException.class));
    }
    try {
      Files.createDirectories(Paths.get("src/test/resources/a/b"));
      Files.delete(directoryAPath);
      fail("It is expected that the previous command generate an exception");
    } catch (IOException ex) {
      assertThat(ex, instanceOf(DirectoryNotEmptyException.class));
      try {
        Files.delete(Paths.get("src/test/resources/a/b"));
        Files.delete(directoryAPath);
      } catch (IOException io) {
        throw new RuntimeException(io);
      }
    }
  }

  @Test
  public void investigatingNewBufferReader() {
    //    Path sourcePath = Paths.get("src/test/resources/test.txt");
    try (Reader br = Files.newBufferedReader(sourcePath); ) {
      assertThat(br, instanceOf(BufferedReader.class));
    } catch (IOException io) {
      throw new RuntimeException(io);
    }
  }

  @Test
  public void investigationNewBufferWriter() {
    Path destinationPath = mockInexistentFile;
    try (Writer wr = Files.newBufferedWriter(destinationPath); ) {
      assertThat(wr, instanceOf(BufferedWriter.class));
    } catch (IOException io) {
      fail(io.getMessage());
    } finally {
      File destinationToDelete = destinationPath.toFile();
      assertThat(destinationToDelete.exists(), equalTo(Boolean.TRUE));
      assertThat(destinationToDelete.delete(), equalTo(Boolean.TRUE));
    }
  }

  @Test
  public void investigationReadAllLines() {
    //    Path sourcePath = Paths.get("src/test/resources/test.txt");
    try {
      List<String> content = Files.readAllLines(sourcePath, Charset.forName("utf-8"));
      assertThat(content, notNullValue());
      assertThat(content, not(empty()));
    } catch (IOException io) {
      throw new RuntimeException(io);
    }
    readAllLinesFailure(Paths.get("/a/b"), NoSuchFileException.class);
    readAllLinesFailure(null, NullPointerException.class);
  }

  private void readAllLinesFailure(Path sourcePath, Class exceptionClassExpected) {
    executeThrowableAction(Files::readAllLines, sourcePath, exceptionClassExpected);
  }

  @Test
  public void checkingIsDirectoryIsFileIsSymbolicLink() {
    //    Path sourcePath = Paths.get("src/test/resources/test.txt");
    assertThat(Files.isDirectory(sourcePath.getParent()), equalTo(Boolean.TRUE));
    assertThat(Files.isDirectory(sourcePath), equalTo(Boolean.FALSE));
    assertThat(Files.isRegularFile(sourcePath), equalTo(Boolean.TRUE));
    assertThat(Files.isRegularFile(sourcePath.getParent()), equalTo(Boolean.FALSE));
    assertThat(Files.isSymbolicLink(sourcePath.getParent()), equalTo(Boolean.FALSE));
    assertThat(Files.isSymbolicLink(sourcePath), equalTo(Boolean.FALSE));
  }

  @Test
  public void checkingIsHidden() {
    //    Path sourcePath = Paths.get("src/test/resources/test.txt");
    try {
      assertThat(Files.isHidden(sourcePath), equalTo(Boolean.FALSE));
    } catch (IOException io) {
      throw new RuntimeException(io);
    }
    if (OSHelper.IS_WINDOWS_OS()) {
      checkingIsHiddenFailure(mockInexistentFile, NoSuchFileException.class);
    }
    checkingIsHiddenFailure(null, NullPointerException.class);
  }

  private void checkingIsHiddenFailure(Path sourceFilePath, Class expectedExceptionClass) {
    executeThrowableAction(Files::isHidden, sourceFilePath, expectedExceptionClass);
  }

  private void executeThrowableAction(
      ConsumerIOThrowable<Path> consumer, Path sourcePath, Class expectedExceptionClass) {
    try {
      consumer.accept(sourcePath);
      // For one test this check fails due the postpone execution.
      fail("It is expected that the previous command generate an exception");
    } catch (IOException | NullPointerException ex) {
      assertThat(ex, instanceOf(expectedExceptionClass));
    }
  }

  @Test
  public void checkingIsReadableIsExecutable() {
    assertThat(Files.isReadable(sourcePath), equalTo(Boolean.TRUE));
    if (OSHelper.IS_WINDOWS_OS()) {
      assertThat(Files.isExecutable(sourcePath), equalTo(Boolean.TRUE));
    } else {
      assertThat(Files.isExecutable(sourcePath), equalTo(Boolean.FALSE));
    }
    sourcePath = Paths.get("/a/b");
    assertThat(Files.isReadable(sourcePath), equalTo(Boolean.FALSE));
    assertThat(Files.isExecutable(sourcePath), equalTo(Boolean.FALSE));
  }

  @Test
  public void checkingSize() {
    try {
      assertThat(Files.size(sourcePath), greaterThan(0L));
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
    executeThrowableAction(Files::size, mockInexistentFile, NoSuchFileException.class);
    executeThrowableAction(Files::size, null, NullPointerException.class);
  }

  @Test
  public void checkingGetLastModifiedTime() {
    try {
      FileTime fileTime = Files.getLastModifiedTime(sourcePath);
      assertThat(fileTime, notNullValue());
      assertThat(fileTime.toMillis(), greaterThan(0L));
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
    executeThrowableAction(
        Files::getLastModifiedTime, mockInexistentFile, NoSuchFileException.class);
    executeThrowableAction(Files::getLastModifiedTime, null, NullPointerException.class);
  }

  @Test
  public void checkingGetOwner() {
    try {
      UserPrincipal principal = Files.getOwner(sourcePath);
      assertThat(principal, notNullValue());
      assertThat(principal.getName(), notNullValue());
      assertThat(principal.getName(), containsString(System.getProperty("user.name")));
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
    executeThrowableAction(Files::getOwner, mockInexistentFile, NoSuchFileException.class);
    executeThrowableAction(Files::getOwner, null, NullPointerException.class);
  }

  @Test
  public void checkingBasicAttribute() {
    try {
      BasicFileAttributes bf = Files.readAttributes(sourcePath, BasicFileAttributes.class);
      assertThat(bf.isDirectory(), equalTo(Boolean.FALSE));
      assertThat(bf.isRegularFile(), equalTo(Boolean.TRUE));
      assertThat(bf.isSymbolicLink(), equalTo(Boolean.FALSE));
      assertThat(bf.isOther(), equalTo(Boolean.FALSE));
      assertThat(bf.size(), greaterThan(0L));
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
    executeThrowableAction(
        p -> Files.readAttributes(p, BasicFileAttributes.class),
        mockInexistentFile,
        NoSuchFileException.class);
    executeThrowableAction(
        p -> Files.readAttributes(p, BasicFileAttributes.class), null, NullPointerException.class);
  }

  @Test
  public void investigatingLinesMethod() {
    try {
      assertThat(Files.lines(sourcePath), instanceOf(Stream.class));
      assertThat(Files.lines(sourcePath).count(), equalTo(2L));
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }

    executeThrowableAction(Files::lines, mockInexistentFile, NoSuchFileException.class);
    executeThrowableAction(Files::lines, null, NullPointerException.class);
  }

  @Test
  public void investigatingWalkMethod() {
    try {
      assertThat(Files.walk(sourcePath), instanceOf(Stream.class));
      assertThat(Files.walk(sourcePath).count(), equalTo(1L));
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
    executeThrowableAction(Files::walk, mockInexistentFile, NoSuchFileException.class);
    executeThrowableAction(Files::walk, null, NullPointerException.class);
  }

  @Test
  public void investigatingListMethod() {
    try {
      assertThat(Files.list(sourcePath.getParent()), instanceOf(Stream.class));
      assertThat(Files.list(sourcePath.getParent()).count(), equalTo(1L));
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
    executeThrowableAction(Files::list, sourcePath, NotDirectoryException.class);
    executeThrowableAction(Files::list, mockInexistentFile, NoSuchFileException.class);
    executeThrowableAction(Files::list, null, NullPointerException.class);
  }

  @Test
  public void investigatingWalk() {
    try {
      assertThat(Files.walk(sourcePath), instanceOf(Stream.class));
      assertThat(Files.walk(sourcePath.getParent()), instanceOf(Stream.class));
      // when is a directory all files and sub-folders are liste as elements in the stream.
      assertThat(Files.walk(sourcePath.getParent()).count(), equalTo(2L));
      // when is a file only ony path returns in the stream
      assertThat(Files.walk(sourcePath).count(), equalTo(1L));
    } catch (IOException io) {
      throw new RuntimeException(io);
    }

    executeThrowableAction(Files::walk, mockInexistentFile, NoSuchFileException.class);
    executeThrowableAction(Files::walk, null, NullPointerException.class);
  }
}
