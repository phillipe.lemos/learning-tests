/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study.nio;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;

import org.junit.BeforeClass;
import org.junit.Test;

import de.study.OSHelper;

/**
 * @author PLemos Path method to be tests : 
 * 1. compareTo - OK 
 * 2. endsWith - OK 
 * 3. equals - OK 
 * 4. getFileName - OK 
 * 5. getFileSystem - OK 
 * 6. getName - OK 
 * 7. getParent - OK 
 * 8. getNameCount - OK
 * 9. getRoot - OK 
 * 10. isAbsolute - OK 
 * 11. iterator  - OK 
 * 12. normalize - OK 
 * 13. of - As Paths.get is a wrapper for Path.of, the tests are in PathsTest.java. - OK 
 * 14. register - ???
 * 15. relativize - OK 
 * 16. resolve  - OK 
 * 17. resolveSimbling   - OK 
 * 18. startWith - OK 
 * 19. subPath - OK 
 * 20. toAbsolutePath - OK 
 * 21. toFile - OK
 * 22. toURI - OK
 */
public class PathStudyTest {

  /**
   * If the path does not exists, the results of isAbsolute is false, even when we point to
   * non-existing file. But when it deals with real paths, the results is different. We also can
   * read from a external variable, line an environment variable that represents a path. With
   * isAbsolute we do not have sure if the path exists or not-
   */
  @Test
  public void understandingIsAbsolute() {
    Path path1;
    if (OSHelper.IS_WINDOWS_OS()) {
      path1 = Paths.get("r:/");
      assertThat(path1.isAbsolute(), equalTo(Boolean.TRUE));
      path1 = Paths.get("c:/", "Users"); // represents a absolute path
      assertThat(path1.isAbsolute(), equalTo(Boolean.TRUE));
      path1 = Paths.get("c:/", "./Users", "/..", "Users/lemos");
      assertThat(path1.isAbsolute(), equalTo(Boolean.TRUE));
      path1 = Paths.get("../Users/lemos");
      assertThat(
          path1.isAbsolute(),
          equalTo(
              Boolean.FALSE)); // here is spected to be a relative path due the lack of the root.
    } else {
      path1 = Paths.get("/a/b/c");
      assertThat(path1.isAbsolute(), equalTo(Boolean.TRUE));
      path1 = Paths.get("/a/b/c/d.txt");
      assertThat(path1.isAbsolute(), equalTo(Boolean.TRUE));
    }
  }

  @Test
  public void investigatingGetRootMethod() {
    Path nonExistingPath = Paths.get("/a/b/c/d");
    Path rootElement = nonExistingPath.getRoot();
    assertThat(rootElement, notNullValue());
    Path moreDifficultToReadPath = Paths.get("../b/././c"); // this represents a no-root ´path
    assertThat(rootElement, equalTo(Paths.get("/")));
    assertThat(moreDifficultToReadPath.normalize(), equalTo(Paths.get("../b/c")));
    rootElement = moreDifficultToReadPath.normalize().getRoot();
    assertThat(rootElement, nullValue());
    moreDifficultToReadPath = Paths.get("./b/././c"); // this also represents a no-root ´path
    assertThat(moreDifficultToReadPath.normalize(), equalTo(Paths.get("b/c")));
    Path nonExistingFile = Paths.get("b.txt");
    assertThat(nonExistingFile, notNullValue());
    assertThat(
        nonExistingFile.toString(), equalTo("b.txt")); // root path from a file is the proper file.
    rootElement = moreDifficultToReadPath.normalize().getRoot();
    assertThat(rootElement, nullValue());
  }

  /** The getParent method does not evaluate the path existence. */
  @Test
  public void investigatingGetParentMethod() {
    Path nonExistingPath = Paths.get("/a/b/c/d");
    Path parent = nonExistingPath.getParent();
    assertThat(parent, notNullValue());
    assertThat(parent, equalTo(Paths.get("/a/b/c")));
    assertThat(parent.getParent(), equalTo(Paths.get("/a/b")));
    assertThat(parent.getParent().getParent(), equalTo(Paths.get("/a")));
    // This express a non-parent path. Only the name of a file.
    nonExistingPath = Paths.get("d.txt");
    assertThat(nonExistingPath.getParent(), nullValue());
    parent = Paths.get("./a").getParent();
    assertThat(parent, notNullValue()); // this the parent is the current path
    assertThat(parent.toString(), equalTo(".")); // this the parent is the current path
    parent = Paths.get("../a").getParent();
    assertThat(parent, notNullValue()); // this the parent is the current path
    assertThat(parent.toString(), equalTo("..")); // this the parent is the current path
  }

  /** This method also does not imply in a real path. */
  @Test
  public void investigatingNamesCountMethod() {
    Path nonExistingPath = Paths.get("/a/b/c/d");
    int expected = 4;
    while (expected >= 0) {
      assertThat(nonExistingPath.getNameCount(), equalTo(expected));
      nonExistingPath = nonExistingPath.getParent();
      expected -= 1;
    }
    nonExistingPath = Paths.get("/");
    assertThat(nonExistingPath.getNameCount(), equalTo(0)); // for root elements the return is zero.
  }

  /** Again, this method does not rely in the real path */
  @Test
  public void investigatingGetNameMethod() {
    String myPatths = "/a/b/c/d";
    Path nonExistingPath = Paths.get(myPatths);
    int index = nonExistingPath.getNameCount();
    testIllegalCases(index, nonExistingPath);
    testIllegalCases(-1, nonExistingPath);
    String[] paths = myPatths.split("/");
    for (int i = 1; i < paths.length; i++) {
      assertThat(nonExistingPath.getName(i - 1).toString(), equalTo(paths[i]));
    }
  }

  private void testIllegalCases(int index, Path pathToTest) {
    try {
      pathToTest.getName(index);
      fail("An exception is expected !!!!");
    } catch (Exception e) {
      boolean expected = (e instanceof IllegalArgumentException);
      assertThat(expected, equalTo(Boolean.TRUE));
    }
  }

  /**
   * Here again the methos getFileSystem does not rely in the real path, and accordingly to the
   * javadoc the class FileSystem does not have any method to check if the supplied path exists.
   */
  @Test
  public void investigatingGetFileSystemMethod() {
    Path nonExistingPath = Paths.get("/a/b/c/d");
    FileSystem fileSystem = nonExistingPath.getFileSystem();
    assertThat(fileSystem, notNullValue());
  }

  @Test
  public void investigatingFileName() {
    Path nonExistingPath = Paths.get("/a/b/c/d");
    Path file = nonExistingPath.getFileName();
    assertThat(file, notNullValue());
    assertThat(file.toString(), equalTo("d"));
  }

  @Test
  public void investigatingEndsWith() {
    Path nonExistingPath = Paths.get("/a/b/c/d");
    assertThat(nonExistingPath.endsWith("d"), equalTo(Boolean.TRUE));
    assertThat(nonExistingPath.endsWith("a"), equalTo(Boolean.FALSE));
    assertThat(nonExistingPath.endsWith(Paths.get("d")), equalTo(Boolean.TRUE));
    assertThat(nonExistingPath.endsWith(Paths.get("/a")), equalTo(Boolean.FALSE));
    assertThat(Paths.get("/a/b../c").endsWith(Paths.get("c")), equalTo(Boolean.TRUE));
    assertThat(Paths.get("/a/b../c").endsWith(Paths.get("b")), equalTo(Boolean.FALSE));
    assertThat(Paths.get("a/b../c").endsWith(Paths.get("c")), equalTo(Boolean.TRUE));
    assertThat(Paths.get("a/b/../c").endsWith(Paths.get("a/c")), equalTo(Boolean.FALSE));
    assertThat(Paths.get("a/b/../c").normalize().endsWith(Paths.get("a/c")), equalTo(Boolean.TRUE));
  }

  @Test
  public void investigaintRelativiseMethod() {
    assertThat(Paths.get("/a/b/c").relativize(Path.of("/a/b/d")), equalTo(Paths.get("../d")));
    assertThat(Path.of("/a/b/d").relativize(Paths.get("/a/b/c")), equalTo(Paths.get("../c")));
    // Root path and non root path
    tryingToRelativiseRootPathAndRelativePath(Paths.get("/a/b/c"), Paths.get("g.txt"));
    tryingToRelativiseRootPathAndRelativePath(Paths.get("g.txt"), Paths.get("/a/b/c"));
    assertThat(Paths.get("/a/c/b").relativize(Paths.get("/a/c/b")), equalTo(Paths.get("")));
    // here it is consider the current directory a parent of a and x. So from c we need
    // go to the parent and then follow until w.
    assertThat(
        Paths.get("a/b/c").relativize(Paths.get("x/y/w")), equalTo(Paths.get("../../../x/y/w")));
  }
	
	/**
	 * If the seconds path is a relative path, then the last segment from the first path is replaced,
	 * otherwise, if the second path is a absolute path, then the first path is ignored and the result
	 * will be the second path.
	 * if the second parameter is an empty path, then the result will be the root from the first parameter
	 * or an empty path.
	 */
	@Test
	public void investigatingResolveSimbling(){
		assertThat(Paths.get("/a/b/c").resolveSibling(Paths.get("d")), equalTo(Paths.get("/a/b/d")));
		assertThat(Paths.get("/a/b/d").resolveSibling(Paths.get("d")), equalTo(Paths.get("/a/b/d")));
		assertThat(Paths.get("/a/b/c/../..").resolveSibling(Paths.get("d")), equalTo(Paths.get("/a/b/c/../d")));
		assertThat(Paths.get("/a/b/c").resolveSibling(Paths.get("/d")), equalTo(Paths.get("/d")));
		assertThat(Paths.get("/a/b/c").resolveSibling(Paths.get("")), equalTo(Paths.get("/a/b")));
		assertThat(Paths.get("/a").resolveSibling(Paths.get("")), equalTo(Paths.get("/")));
		assertThat(Paths.get("a").resolveSibling(Paths.get("")), equalTo(Paths.get("")));
		
	}
  
	private void tryingToRelativiseRootPathAndRelativePath(Path path1, Path path2) {
    try {
      path1.relativize(path2);
      fail("An exception is expected !!!!");
    } catch (Exception e) {
      boolean expected = (e instanceof IllegalArgumentException);
      assertThat(expected, equalTo(Boolean.TRUE));
    }
  }
	
	/**
	 * If the second path is a relative path, the result will be the second path append in the first one.
	 * If the second path is a absolute path, than the first one is ignored and the result is the second
	 * path.
	 */
	@Test
	public void investigatingResolveMethod() {
		assertThat( Paths.get("/a/b/c").resolve("d"), equalTo(Paths.get("/a/b/c/d")));
		assertThat( Paths.get("a/b/c").resolve("d"),  equalTo(Paths.get("a/b/c/d")));
		assertThat( Paths.get("a/b/c").resolve("d/c/b"),  equalTo(Paths.get("a/b/c/d/c/b")));
		assertThat( Paths.get("a/b/c/../..").resolve("d"),  equalTo(Paths.get("a/b/c/../../d")));
		assertThat( Paths.get("a/b/c/../..").normalize().resolve("d"),  equalTo(Paths.get("a/d")));
		// if the path could not be join, than the first part seems to be discarted.
		assertThat( Paths.get("/a/b").resolve("/a/c"), equalTo(Paths.get("/a/c")));
		assertThat( Paths.get("/a/b").resolve("/c"), equalTo(Paths.get("/c"))); 
		assertThat( Paths.get("/a/b/c").resolve(Paths.get("/a/b/d")), equalTo(Paths.get("/a/b/d")));
	}

	
  /** The test is more like a subset test. */
  @Test
  public void investigaringStartWithMethod() {
    Path initial = Paths.get("/a/b/c");
    Path other = Paths.get("/a");
    assertThat(initial.startsWith(other), equalTo(Boolean.TRUE));
    other = Paths.get("/a/f");
    assertThat(initial.startsWith(other), equalTo(Boolean.FALSE));
    other = Paths.get("/a/b");
    assertThat(initial.startsWith(other), equalTo(Boolean.TRUE));
    other = Paths.get("/a/b/d");
    assertThat(initial.startsWith(other), equalTo(Boolean.FALSE));
		assertThat(Paths.get("/a").startsWith(Paths.get("/b")), equalTo(Boolean.FALSE));
		assertThat(Paths.get("/a/b").startsWith(Paths.get("b")), equalTo(Boolean.FALSE));
		assertThat(Paths.get("a/b").startsWith(Paths.get("a")), equalTo(Boolean.TRUE));
  }

  /**
   * Here we have an interesting thing. A Path.subpath(0, getNameCount) is not equals to the
   * original path. This is expected because it returns a relative path. This is important
   */
  @Test
  public void investigatingSubPathMethod() {
		Path initial = Paths.get("/a/b/c");
		assertThat(initial.getNameCount(), equalTo(3));
    assertThat( getSubPaths(initial, 0, initial.getNameCount()), equalTo(Paths.get("a/b/c")));
    assertThat( getSubPaths(initial, 1, initial.getNameCount()), equalTo(Paths.get("b/c")));
		assertThat( getSubPaths(initial, 2, initial.getNameCount()), equalTo(Paths.get("c")));
		assertThat( getSubPaths( initial, 0, 1), equalTo(Paths.get("a")));
		gettingAnExceptionWhileGettingSubPath( initial, -1, 0 );
		gettingAnExceptionWhileGettingSubPath( initial, 2, 0 );
  }
	
	private Path getSubPaths( Path pathInitial, int startIndex, int endIndex ) {
		return pathInitial.subpath(startIndex, endIndex);
	}
	
	public void gettingAnExceptionWhileGettingSubPath(Path pathInitial, int startIndex, int endIndex ){
		try {
			getSubPaths( pathInitial, startIndex, endIndex );
			fail("It is expected an IllegalArgumentException");
		} catch(Exception exception){
			assertThat( exception instanceof IllegalArgumentException, equalTo(Boolean.TRUE));
		}
	}

  /**
   * toAbsolutePath resolves the path using implementation system dependent. This means that for
   * Windows will add the letter drive in front of the path if the path is not the absolute path.
   */
  @Test
  public void investigatingAbsolutePath() {
    Path initial = Paths.get(System.getProperty("java.home"));
    Path absolutePath = initial.toAbsolutePath();
    assertThat(initial, equalTo(absolutePath));
    // this is system dependent
		// here is important to normilize otherwise we'll have the b
		// subfolder information.
		initial = Paths.get("/a/b/../c/d.txt").normalize().toAbsolutePath(); 
		if (OSHelper.IS_WINDOWS_OS()) {
			Path expected = Path.of("c:/a/c/d.txt");
			assertThat(initial, equalTo(expected));
		} else {
			Path expected = Path.of("/a/c/d.txt");
			assertThat(initial, equalTo(expected));
		}
    try {
      Path.of(null).toAbsolutePath();
      fail("An exception is expected !!!!");
    } catch (Exception e) {
      boolean expectedResult = (e instanceof NullPointerException);
      assertThat(expectedResult, equalTo(Boolean.TRUE));
    }
  }

  /**
   * Different from another method from Path class, toRealPath try to find the equivalent real path
   * in the current file system. If it's not possible an exception is thrown.
   *
   * @throws IOException
   */
  @Test
  public void inveestigatingToRealPathMethod() throws IOException {
    Path initial;
    if (OSHelper.IS_WINDOWS_OS()) {
       initial = Paths.get("c:/a/b/c");
    } else {
        initial = Paths.get("/a/b/c");
    }
    try {
      initial.toRealPath(LinkOption.NOFOLLOW_LINKS);
      fail("An exception should be throw");
    } catch (IOException e) {
      boolean expected = e instanceof IOException;
      assertThat(expected, equalTo(Boolean.TRUE));
    }

    String javaPath = System.getProperty("java.home");
    initial = Paths.get(javaPath);
    Path realPath = initial.toRealPath(LinkOption.NOFOLLOW_LINKS);
    assertThat(realPath, notNullValue());
    assertThat(realPath.toString(), equalTo(new File(javaPath).getAbsolutePath()));
  }

  @Test
  public void understandingCompareToMethod() {
    Path path1 = Paths.get("/a", "/b", "/c");
    Path path2 = Paths.get("/a/b/c");
    int resultCompare = path1.compareTo(path2);
    assertThat(resultCompare, equalTo(0));
    path1 = Paths.get("/a");
    path2 = Paths.get("/b");
    resultCompare = path1.compareTo(path2);
    assertThat(resultCompare, lessThan(0));
    resultCompare = path2.compareTo(path1);
    assertThat(resultCompare, greaterThan(0));
  }

  @Test
  public void comparingFilesEquals() {
    Path path1 = Paths.get("/a/b/c");
    Path path2 = Paths.get("/a", "./b../a/b/c");
    // here the paths are compared as they are. So here
    int resultCompare = path1.compareTo(path2);
    assertThat(resultCompare, not(equalTo(0)));
  }

  @Test
  public void comparingNormalizedPaths() {
    Path path1 = Paths.get("/a/b/c");
    Path path2 = Paths.get("/a", "./b../a/b/c");
    // So when they are normilized all the redundancy is eliminated.
    int resultCompare = path1.normalize().compareTo(path2.normalize());
    assertThat(resultCompare, not(equalTo(0)));
    assertThat(Paths.get("/a/b/../c").normalize(), equalTo(Paths.get("/a/c")));
    assertThat(Paths.get("/a/b/c/../../././d").normalize(), equalTo(Paths.get("/a/d")));
  }
	
	@Test
	public void investigatingToFileMethod(){
		File file = Paths.get(System.getProperty("java.home")).toFile();
		assertThat( file, notNullValue());
	}
	
	@Test
	public void investigatingToUriMethod(){
		File file = Paths.get(System.getProperty("java.home")).toFile();
		assertThat( file, notNullValue());
		URI uri = Paths.get(System.getProperty("java.home")).toUri();
		assertThat( uri, notNullValue());
		assertThat( uri, equalTo(file.toURI()));
	}
	
	
	@Test
	public void investigatingIteratormethod(){
		String pathRepresentation = "/a/b/c/d";
		Iterator<Path> pathIterator = Paths.get(pathRepresentation).iterator();
		assertThat(pathIterator, notNullValue());
		String[] segments = pathRepresentation.split("/");
    pathIterator.forEachRemaining( pathElement -> {
			boolean found = false;
			for(String segment : segments) {
				if(pathElement.toString().equals(segment)){
					found = true;
					break;
				}
			}
			if(!found){
				fail("It is expected all segments in the iterator !");
			}
		});
		//root element is never in the iterator.
    pathIterator.forEachRemaining(f -> assertThat(f, not( equalTo(Paths.get("/")))));
	}
	
	
	
	
}
