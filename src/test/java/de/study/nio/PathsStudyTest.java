/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.study.nio;

import java.net.URI;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import org.junit.Test;

/** @author PLemos */
public class PathsStudyTest {

  /** The Paths.get requires the schema information in the URI that is supplied as a parameter. */
  @Test
  public void checkingGetURI() {
    URI uri = URI.create("file:///a/b/c/d");
    assertThat(Paths.get(uri), notNullValue());
    assertThat(Paths.get(uri) instanceof Path, equalTo(Boolean.TRUE));
  }

  @Test(expected = IllegalArgumentException.class)
  public void missingSchemaInformation() {
    URI uri = URI.create("/a/b/c/d");
    Paths.get(uri);
  }

  @Test(expected = NullPointerException.class)
  public void supplyingNullParameter() {
    Paths.get(null);
  }

//  @Test(expected = InvalidPathException.class)
//  public void invalidPathWithSpecialCharacters() {
//    Paths.get("ßßßß?????>><", "???====)()()()()");
//  }

  @Test
  public void creatingPathUsingValidSegments() {
    assertThat(Paths.get("/a", "/b", "/c"), notNullValue());
  }
}
