/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.study.exception;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author PLemos
 */
public class AutoCloseStudyTest {

    private StringBuilder sbResult;

    @Before
    public void setUp() {
        sbResult = new StringBuilder("StringBuilder created");
    }
    
    @After
    public void tearDown(){
        sbResult.delete(0, sbResult.length());
        sbResult = null;
    }
    
    @Test
    public void correctSequenceAutoCloseInstance() {
        builTryCatchFinallyBlock(null, null);
        compareResults(new StringBuilder("StringBuilder created")
                .append("Within the try clause")
                .append("Within the close method")
                .append("Within the finally clause"), sbResult);
    }

    @Test
    public void autoCloseableSequenceWhenThrow() {
        builTryCatchFinallyBlock( new RuntimeException(), null);
        compareResults(new StringBuilder("StringBuilder created")
                .append("Within the try clause")
                .append("Within the close method")
                .append("Within the catch clause")
                .append("Within the finally clause"), sbResult);
    }
    
    @Test
    public void autoCloseableWithExceptionInCloseMethod(){
        builTryCatchFinallyBlock(null, new Exception());
        compareResults(new StringBuilder("StringBuilder created")
                .append("Within the try clause")
                .append("Within the close method")
                .append("Throw inside close method")
                .append("Within the catch clause")
                .append("Within the finally clause"), sbResult);
    }
    
    @Test
    public void autoCloseableWithExceptionInCloseMethodAndInTryMethod(){
        builTryCatchFinallyBlock(new RuntimeException(), new Exception());
        compareResults(new StringBuilder("StringBuilder created")
                .append("Within the try clause")
                .append("Within the close method")
                .append("Throw inside close method")
                .append("Within the catch clause")
                .append("Within the finally clause"), sbResult);
    }

    
    private void builTryCatchFinallyBlock(Exception exceptionToBeThrownInTryBlock, Exception exceptionToBeThrownInClose) {
        try (CloseableInstance c = new CloseableInstance(sbResult, exceptionToBeThrownInClose);) {
            sbResult.append("Within the try clause");
            if(exceptionToBeThrownInTryBlock != null){
                throw exceptionToBeThrownInTryBlock;
            }
        } catch (Exception exception) {
            sbResult.append("Within the catch clause");
            if(exceptionToBeThrownInTryBlock != null && exceptionToBeThrownInClose != null) {
                for(Throwable t : exception.getSuppressed()){
                    assertThat(t, equalTo(exceptionToBeThrownInClose)); 
                }
            } else if (exceptionToBeThrownInTryBlock == null && exceptionToBeThrownInClose != null ){
                assertThat(exception, equalTo(exceptionToBeThrownInClose));
            }
        } finally {
            sbResult.append("Within the finally clause");
        }
    }
    
    private void compareResults(StringBuilder expected, StringBuilder Result) {
        assertThat(expected.toString(), equalTo(Result.toString()));
    }

}
