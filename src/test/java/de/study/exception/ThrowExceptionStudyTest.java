/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.study.exception;

import java.io.IOError;
import java.io.IOException;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import org.junit.Test;

/**
 *
 * @author PLemos 1. Throw a parent exception and catch a parent exception 2.
 * Throw a child exception and catch via parent exception 3. Throw a grandchild
 * exception and catch via parent exception and grandparent exception 4.
 */
public class ThrowExceptionStudyTest {

    @Test(expected = CustomException.class)
    public void throwParentExceptionAndCaptureParentException() throws Exception {
        throw new CustomException();
    }

    @Test(expected = CustomException.class)
    public void throwChildExceptionAndCaptureByParentException() throws Exception {
        throw CustomException.createCustomSom();
    }

    @Test(expected = CustomException.CustomExceptionSon.class)
    public void throwChildexceptionAndCaptureByChildException() throws Exception {
        throw CustomException.createCustomSom();
    }

    @Test(expected = CustomException.class)
    public void thowGrandSonExceptionAndCaptureByGrandParentException() throws Exception {
        throw CustomException.createCustomExceptionGrandSon();
    }

    @Test(expected = CustomException.CustomExceptionSon.class)
    public void thowGrandSonExceptionAndCaptureByChildException() throws Exception {
        throw CustomException.createCustomExceptionGrandSon();
    }

    @Test(expected = CustomException.CustomExceptionGrandSon.class)
    public void throwGrandSonExceptionCapturingByGranSonException() throws Exception {
        throw CustomException.createCustomExceptionGrandSon();
    }

    @Test
    public void usingMultiCastWithCustoExceptionAndHisSon() {
        processComparition("Catch by CustomException.CustomExceptionGrandSon", CustomException.createCustomExceptionGrandSon());
        processComparition("Catch by CustomException.CustomExceptionSon",  CustomException.createCustomSom());
        processComparition("Catch by CustomException", new CustomException());
        processComparition("Catch by Exception or Error", new RuntimeException());
        processComparition("Catch by Exception or Error", new NullPointerException());
        processComparition("Catch by Exception or Error", new IllegalAccessException());
        processComparition("Catch by Exception or Error", new ArithmeticException());
        processComparition("Catch by Exception or Error", new Exception());
        processComparition("Catch by Exception or Error", new IOError(new IOException()));
        processComparition("Catch by Exception or Error", new AssertionError());
    }
    
    private void processComparition( String expectedMessage, Throwable exception ) {
        StringBuilder sbResult = new StringBuilder();
        checkException(sbResult, exception);
        assertThat(sbResult.toString(), equalTo(expectedMessage));
    }
    
    private void checkException( StringBuilder sb, Throwable e ) {
        try {
            throw e;
        } catch (CustomException.CustomExceptionGrandSon e1) {
            sb.append("Catch by CustomException.CustomExceptionGrandSon");
        } catch (CustomException.CustomExceptionSon e2) {
            sb.append("Catch by CustomException.CustomExceptionSon");
        } catch (CustomException e3) {
            sb.append("Catch by CustomException");
        } catch(Exception | Error e4) { // this can be done due the relationship from Exception and Error, they are simblings.
            sb.append("Catch by Exception or Error");
        } catch(Throwable et) {
            sb.append("Catch by Throwable");
        }
    }

}
