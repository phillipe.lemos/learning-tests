/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.study;

/**
 *
 * @author PLemos
 * @param <T>
 */
@FunctionalInterface
public interface ConsumerThrowable<T>  {
    
    void accept(T t) throws Exception;
    
}
