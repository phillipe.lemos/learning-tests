/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study.functionalPrograming;

import de.study.functionalPrograming.Config.ConfigException;
import java.util.Optional;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import org.junit.Before;
import org.junit.Test;

/** @author PLemos */
public class ConfigTest {
	

	private Config configuration;
	
	@Before
	public void setUp(){
		configuration = new Config();
		configuration.put("key1", "value1");
		configuration.put("key2", "value2");
	}
	
	@Test
	public void retrieveValueInConfigUsingGetConfigString(){
    assertThat(configuration.getConfigString("key1"), equalTo(Optional.of("value1")));
		assertThat(configuration.getConfigString("key2"), equalTo(Optional.of("value2")));
	}

	@Test
	public void retrieveValueFromGetPropertyWithoutException(){
		assertThat(configuration.getPropertyWithoutException("key1"), equalTo("value1"));
		assertThat(configuration.getPropertyWithoutException("key2"), equalTo("value2"));
	}
	
  @Test(expected = ConfigException.class)
  public void nonExistingShouldTriggerAnException() {
		configuration.getConfigString("asdasdas");
	}
	
	@Test
	public void nonExistingValueReturningNull(){
		assertThat(configuration.getPropertyWithoutException("asdasda"), nullValue());
	}
	
	@Test
	public void nonExistingValueAndNotThrowsException(){
		assertThat(configuration.getConfigStringUsingSupplier("adasdsa"), equalTo( Optional.empty() ));
	}
}
