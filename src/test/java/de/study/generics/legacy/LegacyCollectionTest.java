/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study.generics.legacy;

import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.fail;
import org.junit.Test;

/** @author PLemos */
public class LegacyCollectionTest {

  private void iterateOverTheCollectionAndConvertingToString(List list) {
    for (Object obj : list) {
      String s = (String) obj;
    }
  }

  @Test
  public void withoutWorkingWithGenericsExceptionCouldHappen() {
    List list = new ArrayList();
    list.add(1);
    list.add("My string");
    list.add(2.5);
    try {
      iterateOverTheCollectionAndConvertingToString(list);
      fail("It is expected an exception in this point");
    } catch (Exception ex) {
      assertThat(ex, instanceOf(ClassCastException.class));
    }
  }
}
