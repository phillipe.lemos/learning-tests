/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study.generics;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.IsNot.not;
import org.junit.Test;

/** @author PLemos */
public class LowerUpperBoundsTest {

  @Test
  public void checkingImmutabilityFromUnboundCollection() {
    // You can see that the list defined by List<? extends NaturalNumber> is not read-only in the
    // strictest sense of the word, but you might think of it that way because you cannot store a
    // new element or change an existing element in the list.
    List<?> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
    assertThat(list.size(), equalTo(4));
    list.remove(1);
    assertThat(list.size(), equalTo(3));
  }

  private List<Integer> getIntegerList() {
    return Arrays.asList(1, 2, 3, 4);
  }

  private List<Number> getNumberList() {
    return Arrays.asList(1, 2, 3, 4);
  }

  private List<? extends Number> getUpperBoundNumberList() {
    return Arrays.asList(1, 2, 3, 4);
  }

  private List<? extends Integer> getUpperBoundIntegerrList() {
    return Arrays.asList(1, 2, 3, 4);
  }

  /**
   * Note about the approach. I used some ideas from
   * http://tutorials.jenkov.com/java-reflection/generics.html and from
   * https://farenda.com/java/java-reflection-generic-type/. Here, I was not able to get the generic
   * information from the class declaration, like List<Integer> returns always List<E>. Getting
   * information from the methods is different and I could get the correct type.
   *
   * @throws Exception
   */
  @Test
  public void checkingInheritenceBetweenGenericCollections() throws Exception {
    List<Number> list2 = getNumberList();
    List<Integer> list3 = getIntegerList();
    assertThat(list2 instanceof List<?>, equalTo(Boolean.TRUE));
    assertThat(list3 instanceof List<?>, equalTo(Boolean.TRUE));

    Method intMethod = this.getClass().getDeclaredMethod("getIntegerList");
    Method numberMethod = this.getClass().getDeclaredMethod("getNumberList");

    ParameterizedType typeMethodInt = (ParameterizedType) intMethod.getGenericReturnType();
    ParameterizedType typeMethodNumber = (ParameterizedType) numberMethod.getGenericReturnType();

    assertThat(typeMethodInt, not(equalTo(typeMethodNumber)));
    assertThat(
        typeMethodInt.getActualTypeArguments()[0],
        not(equalTo(typeMethodNumber.getActualTypeArguments()[0])));
  }
	
	/**
	 * Declaring a list of type List<? super A> means that this list could hold all A instances and his
	 * sub-classes. 
	 */
	@Test
	public void lowerBoundChecking(){
		List<? super Number> myIntList = new ArrayList<>();
		assertThat( myIntList.add(1), equalTo( Boolean.TRUE ) );
    assertThat(myIntList.add(Double.valueOf(2.0)), equalTo(Boolean.TRUE));
		assertThat(myIntList.add(Float.valueOf("2.0")), equalTo(Boolean.TRUE));
		assertThat(myIntList.add(Short.valueOf("2")), equalTo(Boolean.TRUE));
		assertThat(myIntList.add(Byte.valueOf("2")), equalTo(Boolean.TRUE));
		assertThat( myIntList.size(), equalTo(5));
	}
  
}
