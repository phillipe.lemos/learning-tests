/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study.collection;

import static de.study.Message.FAIL_MESSAGE;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.BiFunction;
import java.util.function.Function;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;

/** @author PLemos */
public class MapTest {

	private Map<Integer, Integer> map;

	@Before
	public void setUp() {
		map = new HashMap<>();
	}

	@Test
	public void checkingIsEmpty() {
		assertThat(map.isEmpty(), equalTo(Boolean.TRUE));
		map = Map.of(1, 1);
		assertThat(map.isEmpty(), equalTo(Boolean.FALSE));
	}

	@Test
	public void checkingSize() {
		assertThat(map.size(), equalTo(0));
		map = Map.of(1, 1);
		assertThat(map.size(), equalTo(1));
	}

	@Test
	public void checkingClear() {
		map.put(1, 1);
		assertThat(map.size(), equalTo(1));
		map.clear();
		assertThat(map.size(), equalTo(0));
		try {
			map = Map.of(1, 1);
			map.clear();
			fail(FAIL_MESSAGE);
		} catch (Exception ex) {
			assertThat(ex, instanceOf(UnsupportedOperationException.class));
		}
	}

	@Test
	public void checkingGet() {
		assertThat(map.get(1), nullValue());
		map.put(1, 1);
		assertThat(map.get(1), equalTo(1));
		// Expected failures
		failToObtainValues(new Hashtable<>(), NullPointerException.class, Map<Integer, Integer>::get, null);
		failToObtainValues(new TreeMap<>(), NullPointerException.class, Map<Integer, Integer>::get, null);
	}

	@Test
	public void checkingPut() {
		assertThat(map.put(1, 1), nullValue());
		assertThat(map.put(1, 2), equalTo(1));
		assertThat(map.put(1, 3), equalTo(2));
		// Expected failures
		generateExceptionWhilePutKeyValueInMap(new Hashtable<>(), null, 1, NullPointerException.class);
		generateExceptionWhilePutKeyValueInMap(new TreeMap<>(), null, 1, NullPointerException.class);
		generateExceptionWhilePutKeyValueInMap(Map.of(1, 1), 1, 2, UnsupportedOperationException.class);
	}

	private void generateExceptionWhilePutKeyValueInMap(Map<Integer, Integer> mapSource, Integer key, Integer value,
			Class<? extends Exception> exceptionClass) {
		try {
			mapSource.put(key, value);
			fail(FAIL_MESSAGE);
		} catch (Exception ex) {
			assertThat(ex, instanceOf(exceptionClass));
		}
	}

	@Test
	public void checkingRemove() {
		map.put(1, 1);
		map.put(2, 3);
		assertThat(map.remove(1), equalTo(1));
		assertThat(map.remove(2), equalTo(3));
		assertThat(map.remove(2), nullValue());
		// Expected failures
		failToObtainValues(new Hashtable<>(), NullPointerException.class, Map<Integer, Integer>::remove, null);
		failToObtainValues(new TreeMap<>(), NullPointerException.class, Map<Integer, Integer>::remove, null);
	}

	private void failToObtainValues(Map<Integer, Integer> dataSource, Class<? extends Exception> exceptionClass,
			BiFunction<Map, Integer, ?> func, // changing the return type from Integer to any kind of Object I can use
												// more broad
			Integer key) {
		try {
			func.apply(dataSource, key);
			fail(FAIL_MESSAGE);
		} catch (Exception ex) {
			assertThat(ex, instanceOf(exceptionClass));
		}
	}

	@Test
	public void checkingContainsKey() {
		assertThat(map.containsKey(100), equalTo(Boolean.FALSE));
		map.put(1, 2);
		assertThat(map.containsKey(1), equalTo(Boolean.TRUE));
		// Expected failures
		failToObtainValues(new TreeMap<>(), NullPointerException.class, Map<Integer, Integer>::containsKey, null);
	}

	@Test
	public void checkingContainsValue() {
		assertThat(map.containsValue(1), equalTo(Boolean.FALSE));
		map.put(1, 10);
		assertThat(map.containsValue(10), equalTo(Boolean.TRUE));
		// Expected failures
		failToObtainValues(new Hashtable<>(), NullPointerException.class, Map<Integer, Integer>::containsValue, null);
	}

	@Test
	public void checkingKeySet() {
		assertThat(map.keySet(), instanceOf(Set.class));
	}

	@Test
	public void checkingValues() {
		assertThat(map.values(), instanceOf(Collection.class));
	}

	@Test
	public void checkingReplace() {
		assertThat(map.replace(1, 1), nullValue());
		map.put(1, 1);
		assertThat(map.replace(1, 2), equalTo(1));
		// Expected failures
		failToReplaceValueInMap(new TreeMap<>(), NullPointerException.class, null, 1);
		failToReplaceValueInMap(new Hashtable<>(), NullPointerException.class, null, 1);
		failToReplaceValueInMap(Map.of(1, 2), UnsupportedOperationException.class, 1, 3);
	}

	private void failToReplaceValueInMap(Map<Integer, Integer> mapSource, Class<? extends Exception> exceptionClass,
			Integer key, Integer value) {
		try {
			mapSource.replace(key, value);
			fail(FAIL_MESSAGE);
		} catch (Exception ex) {
			assertThat(ex, instanceOf(exceptionClass));
		}
	}

	@Test
	public void checkingReplaceKeyOldNewValue() {
		assertThat(map.replace(1, 1, 3), equalTo(Boolean.FALSE));
		map.put(1, 1);
		assertThat(map.replace(1, 1, 2), equalTo(Boolean.TRUE));
		// Expected failures
		failToReplaceOldValueWithNewValueInMap(new TreeMap<>(), NullPointerException.class, null, 1, 1);
		failToReplaceOldValueWithNewValueInMap(new Hashtable<>(), NullPointerException.class, null, 1, 1);
		failToReplaceOldValueWithNewValueInMap(new Hashtable<>(), NullPointerException.class, 1, null, 1);
		failToReplaceOldValueWithNewValueInMap(new Hashtable<>(), NullPointerException.class, 1, 1, null);
		failToReplaceOldValueWithNewValueInMap(Map.of(1, 2), UnsupportedOperationException.class, 1, 1, 3);
	}

	private void failToReplaceOldValueWithNewValueInMap(Map<Integer, Integer> mapSource,
			Class<? extends Exception> exceptionClass, Integer key, Integer oldValue, Integer newValue) {
		try {
			mapSource.replace(key, oldValue, newValue);
			fail(FAIL_MESSAGE);
		} catch (Exception ex) {
			assertThat(ex, instanceOf(exceptionClass));
		}
	}

	@Test
	public void checkingReplaceAll() {
		assertThat(map.put(1, 1), nullValue());
		assertThat(map.put(2, 3), nullValue());
		map.replaceAll((k, v) -> v * 2);
		assertThat(map.get(1), equalTo(2));
		assertThat(map.get(2), equalTo(6));
		// Expected failures
		failToReplaceAllWithFunction(map, null, NullPointerException.class);
		failToReplaceAllWithFunction(new TreeMap<>(), null, NullPointerException.class);
		failToReplaceAllWithFunction(new Hashtable<>(), null, NullPointerException.class);
		failToReplaceAllWithFunction(Map.of(1, 1), (k, v) -> v * 2, UnsupportedOperationException.class);
	}

	private void failToReplaceAllWithFunction(Map<Integer, Integer> sourceMap,
			BiFunction<Integer, Integer, Integer> biFunction, Class<? extends Exception> exceptionClass) {
		try {
			sourceMap.replaceAll(biFunction);
			fail(FAIL_MESSAGE);
		} catch (Exception ex) {
			assertThat(ex, instanceOf(exceptionClass));
		}
	}

	@Test
	public void checkingPutAll() {
		assertThat(map.isEmpty(), equalTo(Boolean.TRUE));
		map.putAll(Map.of(1, 2, 2, 3));
		assertThat(map.get(1), equalTo(2));
		assertThat(map.get(2), equalTo(3));
		// Expected failures
		failToPutAll(map, NullPointerException.class, null);
		failToPutAll(new TreeMap<>(), NullPointerException.class, null);
		failToPutAll(new Hashtable<>(), NullPointerException.class, null);
		failToPutAll(Map.of(1, 2), UnsupportedOperationException.class, Map.of(1, 2, 2, 3));
		failToPutAll(Collections.unmodifiableMap(map), UnsupportedOperationException.class, Map.of(1, 2, 2, 3));
	}

	private void failToPutAll(Map<Integer, Integer> mapDestination, Class<? extends Exception> exceptionClass,
			Map<Integer, Integer> mapSource) {
		try {
			mapDestination.putAll(mapSource);
			fail(FAIL_MESSAGE);
		} catch (Exception ex) {
			assertThat(ex, instanceOf(exceptionClass));
		}
	}

	@Test
	public void checkintPutIfAbsent() {
		assertThat(map.putIfAbsent(1, 2), nullValue());
		assertThat(map.putIfAbsent(1, 3), equalTo(2));
		assertThat(map.get(1), equalTo(2));
		map.put(2, null);
		assertThat(map.putIfAbsent(2, 4), nullValue());
		assertThat(map.get(2), equalTo(4));
		// Expected failures
		failToPutifAbsent(null, NullPointerException.class, 1, 3);
		failToPutifAbsent(new Hashtable<>(), NullPointerException.class, null, 3);
		failToPutifAbsent(Map.of(1, 2), UnsupportedOperationException.class, null, 3);
		failToPutifAbsent(Collections.unmodifiableMap(map), UnsupportedOperationException.class, null, 3);
	}

	private void failToPutifAbsent(Map<Integer, Integer> mapSource, Class<? extends Exception> exceptionClass,
			Integer key, Integer value) {
		try {
			mapSource.putIfAbsent(key, value);
			fail(FAIL_MESSAGE);
		} catch (Exception ex) {
			assertThat(ex, instanceOf(exceptionClass));
		}
	}

	/**
	 * The method computeIfAbsent returns the new value computed if the key is does
	 * not exists or the value is null.
	 * 
	 * The computeIfAbsent is equivalent to : if (map.get(key) == null) { V newValue
	 * = mappingFunction.apply(key); if (newValue != null) map.put(key, newValue); }
	 * 
	 */
	@Test
	public void checkingComputeIfAbsent() {
		Function<Integer, Integer> func = k -> 10;
		assertThat(map.isEmpty(), equalTo(Boolean.TRUE));
		assertThat(map.computeIfAbsent(1, func), equalTo(10));
		assertThat(map.get(1), equalTo(10));
		map.put(2, null);
		assertThat(map.computeIfAbsent(2, func), equalTo(10));
		assertThat(map.get(2), equalTo(10));
		// Expected failures
		failComputeIfAbsent(null, NullPointerException.class, func);
		failComputeIfAbsent(map, NullPointerException.class, null);
		failComputeIfAbsent(Map.of(3, 4), UnsupportedOperationException.class, func);
		failComputeIfAbsent(Collections.unmodifiableMap(map), UnsupportedOperationException.class, func);
	}

	private void failComputeIfAbsent(Map<Integer, Integer> mapSource, Class<? extends Exception> exceptionClass,
			Function<Integer, Integer> func) {
		try {
			mapSource.computeIfAbsent(1, func);
			fail(FAIL_MESSAGE);
		} catch (Exception ex) {
			assertThat(ex, instanceOf(exceptionClass));
		}
	}

	/**
	 * The method computeIfPresent returns the new value. If the Bifunction returns
	 * null the key is removed from the the map. If the key does not exists, the
	 * function returns nulls.
	 * 
	 * The computeIfPresent is equivalent to if (map.get(key) != null) { V oldValue
	 * = map.get(key); V newValue = remappingFunction.apply(key, oldValue); if
	 * (newValue != null) map.put(key, newValue); else map.remove(key); }
	 */
	@Test
	public void checkingComputeIfPresent() {
		BiFunction<Integer, Integer, Integer> func = (k, v) -> 10 * v;
		assertThat(map.isEmpty(), equalTo(Boolean.TRUE));
		map.put(1, 3);
		assertThat(map.computeIfPresent(1, func), equalTo(30));
		assertThat(map.get(1), equalTo(30));
		// removing an element when the BiFunction return null
		assertThat(map.computeIfPresent(1, (k, v) -> null), nullValue());
		assertThat(map.isEmpty(), equalTo(Boolean.TRUE));
		// Checking inexistent value in the map, as expected inexisted value it isn't
		// added.
		map.put(1, 3);
		assertThat(map.computeIfPresent(2, (k, v) -> v * 50), nullValue());
		// Expected failures
		failComputeIfPresent(null, NullPointerException.class, func);
		failComputeIfPresent(map, NullPointerException.class, null);
		failComputeIfPresent(new TreeMap<>(), NullPointerException.class, null);
		failComputeIfPresent(new Hashtable<>(), NullPointerException.class, null);
		failComputeIfPresent(Map.of(1, 2), UnsupportedOperationException.class, func);
		failComputeIfPresent(Collections.unmodifiableMap(map), UnsupportedOperationException.class, func);
	}

	private void failComputeIfPresent(Map<Integer, Integer> mapSource, Class<? extends Exception> exceptionClass,
			BiFunction<Integer, Integer, Integer> biFunc) {
		try {
			mapSource.computeIfPresent(1, biFunc);
			fail(FAIL_MESSAGE);
		} catch (Exception ex) {
			assertThat(ex, instanceOf(exceptionClass));
		}
	}

	/**
	 * If the key exists or does not exists and the remapping function returns a
	 * value, then the function will return that value. In the first case, the key
	 * will point to the new value, and in the second case, the key/value will be
	 * inserted into the map. If the mapping function returns null and the map
	 * contains the key, then the this key is removed from the map. This function
	 * fails, when the key does not exists and the key or the value is computed in
	 * the remapping function. Like e.g: key = x does not exists, and we have a
	 * function like (x,v) -> x*10
	 * 
	 * The compute function is equivalent to V oldValue = map.get(key); V newValue =
	 * remappingFunction.apply(key, oldValue); if (oldValue != null) { if (newValue
	 * != null) map.put(key, newValue); else map.remove(key); } else { if (newValue
	 * != null) map.put(key, newValue); else return null; }
	 * 
	 */
	@Test
	public void checkingCompute() {
		assertThat(map.isEmpty(), equalTo(Boolean.TRUE));
		map.put(1, 2);
		assertThat(map.compute(1, (k, v) -> v * 10), equalTo(20));
		map.put(2, 4);
		assertThat(map.compute(2, (k, v) -> v * 10), equalTo(40));
		assertThat(map.compute(3, (k, v) -> 10), equalTo(10));
		assertThat(map.get(3), equalTo(10));
		// If the function returns null, the key is removed, and null is returned.
		assertThat(map.compute(2, (k, v) -> null), nullValue());
		assertThat(map.get(2), nullValue());
		failCompute(map, NullPointerException.class, (k, v) -> 10 * v, 5);
		failCompute(map, NullPointerException.class, null, 6);
		failCompute(null, NullPointerException.class, (k, v) -> 10 * v, 5);
		failCompute(Map.of(1, 2), UnsupportedOperationException.class, (k, v) -> 10 * v, 5);
		failCompute(Collections.unmodifiableMap(map), UnsupportedOperationException.class, (k, v) -> 10 * v, 5);
	}

	private void failCompute(Map<Integer, Integer> mapSource, Class<? extends Exception> exceptionClass,
			BiFunction<Integer, Integer, Integer> mappingFunction, Integer key) {
		try {
			mapSource.compute(key, mappingFunction);
			fail(FAIL_MESSAGE);
		} catch (Exception ex) {
			assertThat(ex, instanceOf(exceptionClass));
		}
	}

	@Test
	public void checkingGetOfDefault() {
		assertThat(map.isEmpty(), equalTo(Boolean.TRUE));
		map.put(1, 2);
		assertThat(map.getOrDefault(1, -1), equalTo(2));
		assertThat(map.getOrDefault(2, -1), equalTo(-1));
		assertThat(map.getOrDefault(null, -1), equalTo(-1)); // here the default value is returned because the map
																// accept null in the key.
		failGetDefault(new Hashtable<>(), NullPointerException.class, null);
		failGetDefault(null, NullPointerException.class, 1);
	}

	private void failGetDefault(Map<Integer, Integer> mapSource, Class<? extends Exception> exceptionClass,
			Integer key) {
		try {
			mapSource.getOrDefault(key, -1);
			fail(FAIL_MESSAGE);
		} catch (Exception ex) {
			assertThat(ex, instanceOf(exceptionClass));
		}
	}

	/**
	 * The merge function change the value from the map if the key is in the map and
	 * the new value is not null. If the mapping function returns null, the value
	 * associate to the key is the newValue, otherwise the newValue will be the
	 * result of the mapping function.
	 * 
	 * The default implementation is equivalent to performing the following steps
	 * for this map, then returning the current value or null if absent: V oldValue
	 * = map.get(key); V newValue = (oldValue == null) ? value :
	 * remappingFunction.apply(oldValue, value); if (newValue == null)
	 * map.remove(key); else map.put(key, newValue);
	 */
	@Test
	public void checkingMerge() {
		map.put(1, 3);
		BiFunction<Integer, Integer, Integer> biFunc = (oldValue, newValue) -> oldValue + newValue;
		assertThat(map.merge(1, 10, biFunc), equalTo(13));
		assertThat(map.merge(2, 10, biFunc), equalTo(10));
		assertThat(map.get(2), equalTo(10));
		assertThat(map.merge(5, 10, (oldValue, newValue) -> null), equalTo(10));
		assertThat(map.get(5), equalTo(10));
		// Delete elements map when the remapping functions returns null to existing key
		// in the map
		assertThat(map.merge(5, 10, (oldValue, newValue) -> null), nullValue());
		assertThat(map.get(5), nullValue());
		// Merge function fails
		failToMerge(null, NullPointerException.class, 1, 1, biFunc);
		failToMerge(map, NullPointerException.class, 1, null, (oldValue, newValue) -> null);
		failToMerge(map, NullPointerException.class, 1, 1, null);
		failToMerge(new TreeMap<>(), NullPointerException.class, 1, 1, null);
		failToMerge(new Hashtable<>(), NullPointerException.class, null, 1, biFunc);
		failToMerge(Map.of(1, 3), UnsupportedOperationException.class, 1, 10, biFunc);
		failToMerge(Collections.unmodifiableMap(map), UnsupportedOperationException.class, 1, 10, biFunc);
	}

	private void failToMerge(Map<Integer, Integer> mapSource, Class<? extends Exception> exceptionClass, Integer key,
			Integer newValue, BiFunction<Integer, Integer, Integer> biFunction) {
		try {
			mapSource.merge(key, newValue, biFunction);
			fail(FAIL_MESSAGE);
		} catch (Exception ex) {
			assertThat(ex, instanceOf(exceptionClass));
		}
	}

}
