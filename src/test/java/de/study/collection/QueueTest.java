/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study.collection;

import static de.study.Message.FAIL_MESSAGE;
import java.util.ArrayDeque;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.function.Supplier;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;

/** @author PLemos */
public class QueueTest {

  private Queue<Integer> initialQueue;

  @Before
  public void setUp() {
    initialQueue = new ArrayDeque<>();
  }

  @Test
  public void checkingOffer() {
    assertThat(initialQueue.offer(1), equalTo(Boolean.TRUE));
    assertThat(initialQueue.offer(3), equalTo(Boolean.TRUE));
    assertThat(initialQueue.offer(5), equalTo(Boolean.TRUE));
    assertThat(initialQueue.size(), equalTo(3));
    try {
      Queue queue = new ArrayDeque<>(2);
      queue.offer(null);
      fail(FAIL_MESSAGE);
    } catch (Exception ex) {
      assertThat(ex, instanceOf(NullPointerException.class));
    }
  }
	
  @Test
  public void checkingPeek() {
    assertThat(initialQueue.peek(), nullValue()); // if the Queue is empty, than return null.
    readElement(initialQueue::peek);
  }

  @Test
  public void checkElement() {
    try {
      assertThat(initialQueue.element(), nullValue()); // if the Queue is empty, than throws NoSuchElementException
    } catch (Exception ex) {
      assertThat(ex, instanceOf(NoSuchElementException.class));
    }
    readElement(initialQueue::element);
  }

  private void readElement(Supplier<Integer> func) {
    assertThat(initialQueue.offer(10), equalTo(Boolean.TRUE));
    assertThat(func.get(), equalTo(10));
    assertThat(initialQueue.size(), equalTo(1));
  }

  @Test
  public void checkingPoll() {
    assertThat(
        (new ArrayDeque<>()).poll(), nullValue()); // if the Queue is empty, than return null.
    assertThat(initialQueue.offer(10), equalTo(Boolean.TRUE));
    assertThat(initialQueue.poll(), equalTo(10)); // poll method remove the item from the Queue
    assertThat(initialQueue.isEmpty(), equalTo(Boolean.TRUE));
  }

  @Test
  public void checkingRemove() {
    assertThat(initialQueue.offer(10), equalTo(Boolean.TRUE));
    assertThat(initialQueue.remove(), equalTo(10));
    assertThat(initialQueue.isEmpty(), equalTo(Boolean.TRUE));
    try {
      initialQueue.remove();
      fail(FAIL_MESSAGE);
    } catch (Exception ex) {
      assertThat(ex, instanceOf(NoSuchElementException.class));
    }
  }
}
