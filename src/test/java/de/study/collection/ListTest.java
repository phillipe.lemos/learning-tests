/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study.collection;

import static de.study.Message.FAIL_MESSAGE;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.fail;
import org.junit.Test;

/** @author PLemos */
public class ListTest {

  private final List<Integer> initialList;

  private final List<Integer> immutableList;
	
  public ListTest() {
    initialList = Arrays.asList(1, 2, 3, 4, 5);
    immutableList = List.of(1, 2, 3, 4, 5);
  }

  @Test
  public void checkAddFromList() {
    addMethodList(new ArrayList<>(initialList));
    addMethodList(new LinkedList<>(initialList));
		failToAddMethodList( initialList, UnsupportedOperationException.class );
		failToAddMethodList( immutableList, UnsupportedOperationException.class );
  }

  private void addMethodList(List<Integer> sourceItems) {
    assertThat(sourceItems.size(), equalTo(5));
    sourceItems.add(0, 0);
    assertThat(sourceItems.size(), equalTo(6));
  }
	
	private void failToAddMethodList(List<Integer> sourceItems, Class< ? extends Exception > exceptionClass ) {
		try {
			sourceItems.add(0, 0);
			fail(FAIL_MESSAGE);
		} catch(Exception ex) {
			assertThat( ex, instanceOf( exceptionClass ) );
		}
	}

  @Test
  public void getMethodFromList() {
    chekingAllElementsInTheList(new ArrayList<>(initialList));
    chekingAllElementsInTheList(new LinkedList<>(initialList));
    chekingInexistentElements(new ArrayList<>(initialList), Integer.MAX_VALUE);
    chekingInexistentElements(new ArrayList<>(initialList), -1);
  }

  private void chekingAllElementsInTheList(List<Integer> sourceItems) {
    for (int i = 1; i <= 5; i++) {
      assertThat(sourceItems.get(i - 1), equalTo(i));
    }
  }

  private void chekingInexistentElements(List<Integer> sourceItems, int position) {
    try {
      sourceItems.get(position);
      fail("It is expected an exception here.");
    } catch (Exception ex) {
      assertThat(ex, instanceOf(IndexOutOfBoundsException.class));
    }
  }

  @Test
  public void checkingIndexOf() {
    indexOfElements(new ArrayList<>(initialList));
    indexOfElements(new LinkedList<>(initialList));
  }

  private void indexOfElements(List<Integer> sourceItems) {
    assertThat(sourceItems.indexOf(1), greaterThan(-1));
    assertThat(sourceItems.indexOf(0), equalTo(-1));
    assertThat(sourceItems.indexOf(Integer.MAX_VALUE), equalTo(-1));
    assertThat(sourceItems.indexOf(Integer.MIN_VALUE), equalTo(-1));
  }

  @Test
  public void checkLastIndexOf() {
    lastIndexOfElements(new ArrayList<>(initialList));
    lastIndexOfElements(new LinkedList<>(initialList));
  }

  private void lastIndexOfElements(List<Integer> sourceItems) {
    sourceItems.add(4);
    assertThat(sourceItems.lastIndexOf(4), greaterThan(-1));
    assertThat(sourceItems.lastIndexOf(0), equalTo(-1));
    assertThat(sourceItems.lastIndexOf(Integer.MAX_VALUE), equalTo(-1));
    assertThat(sourceItems.lastIndexOf(Integer.MIN_VALUE), equalTo(-1));
  }

  @Test
  public void checkRemoveByIndex() {
    removeAndCheck(new ArrayList<>(initialList), 1);
    removeAndCheck(new LinkedList<>(initialList), 1);
    failRemoveByIndexDueInexistentPosition(new ArrayList<>(initialList), -1, IndexOutOfBoundsException.class);
    failRemoveByIndexDueInexistentPosition(new ArrayList<>(initialList), Integer.MAX_VALUE, IndexOutOfBoundsException.class);
    failRemoveByIndexDueInexistentPosition(new ArrayList<>(initialList), Integer.MIN_VALUE, IndexOutOfBoundsException.class);
    failRemoveByIndexDueInexistentPosition(new LinkedList<>(initialList), -1, IndexOutOfBoundsException.class);
    failRemoveByIndexDueInexistentPosition(new LinkedList<>(initialList), Integer.MAX_VALUE, IndexOutOfBoundsException.class);
    failRemoveByIndexDueInexistentPosition(new LinkedList<>(initialList), Integer.MIN_VALUE, IndexOutOfBoundsException.class);
		failRemoveByIndexDueInexistentPosition( initialList, 0, UnsupportedOperationException.class );
		failRemoveByIndexDueInexistentPosition( immutableList, 0, UnsupportedOperationException.class );
  }

  private void removeAndCheck(List<Integer> sourceItems, int index) {
    assertThat(sourceItems.size(), equalTo(5));
    sourceItems.remove(index);
    assertThat(sourceItems.size(), equalTo(4));
  }

  private void failRemoveByIndexDueInexistentPosition(List<Integer> sourceItems, int index, Class< ? extends Exception > classExeption) {
    try {
      sourceItems.remove(index);
      fail(FAIL_MESSAGE);
    } catch (Exception ex) {
      assertThat(ex, instanceOf(classExeption));
    }
  }
	
	@Test
	public void checkingSet(){
		List<Integer> numbers = new ArrayList<>(initialList);
		assertThat( numbers.set(0, 10), equalTo(1));
		assertThat( numbers.get(0), equalTo(10));
		assertThat( initialList.set(0, 10), equalTo(1));
		assertThat( initialList.set(0, 1), equalTo(10));
		tryToSetValueInTheList( numbers, -1 , 10, IndexOutOfBoundsException.class );
		tryToSetValueInTheList( null, -1 , 10, NullPointerException.class );
		tryToSetValueInTheList( immutableList, 0 , 10, UnsupportedOperationException.class );
	}
	
	private void tryToSetValueInTheList( List<Integer> sourceItem, int position, Integer value, Class< ? extends Exception > kindOfException ){
		try {
      sourceItem.set(position, value );
			fail(FAIL_MESSAGE);
		} catch(Exception ex) {
			assertThat( ex, instanceOf( kindOfException ));
		}
	}
	
}
	