/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study.collection;

import java.util.Comparator;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import de.study.ConsumerThrowable;
import static de.study.Message.FAIL_MESSAGE;

/** @author PLemos */
public class NavigableSetTest {

  private final Set<Integer> initialAndImmutableSet;

  private final Comparator<Integer> descendingSortOrder = (n1, n2) -> n2 - n1;

  private NavigableSet<Integer> treeSet;

  private SortedSet<Integer> sortedSet;

  public NavigableSetTest() {
    initialAndImmutableSet = Set.of(3, 1, 2, 5, 4);
  }

  @Before
  public void setUp() {
    treeSet = new TreeSet<>(initialAndImmutableSet);
    sortedSet = new TreeSet<>(initialAndImmutableSet);
  }

  @Test
  public void checkingAscendingSortOrder() {
    int smallNumber = Integer.MIN_VALUE;
    for (Integer number : treeSet) {
      assertThat(number, greaterThan(smallNumber));
      smallNumber = number;
    }
  }

  @Test
  public void checkingDescendingSortOrderTest() {
    int smallNumber = Integer.MAX_VALUE;
    NavigableSet<Integer> localTreeSet = new TreeSet<>(descendingSortOrder);
    localTreeSet.addAll(initialAndImmutableSet);
    for (Integer number : localTreeSet) {
      assertThat(number, lessThan(smallNumber));
      smallNumber = number;
    }
  }

  @Test
  public void checkingLower() {
    assertThat(treeSet.lower(5), equalTo(4));
    assertThat(treeSet.lower(100), equalTo(5));
    assertThat(treeSet.lower(-1), nullValue());
    failToTriggerMethod(null, NullPointerException.class, treeSet::lower);
    failToTriggerMethod(11, NullPointerException.class, null);
  }

  @Test
  public void checkingFloor() {
    assertThat(treeSet.floor(5), equalTo(5));
    assertThat(treeSet.floor(100), equalTo(5));
    assertThat(treeSet.floor(-1), nullValue());
    failToTriggerMethod(null, NullPointerException.class, treeSet::floor);
    failToTriggerMethod(1, NullPointerException.class, null);
  }

  @Test
  public void checkingCeiling() {
    assertThat(treeSet.ceiling(5), equalTo(5));
    assertThat(treeSet.ceiling(1), equalTo(1));
    assertThat(treeSet.ceiling(6), nullValue());
    failToTriggerMethod(null, NullPointerException.class, treeSet::ceiling);
    failToTriggerMethod(1, NullPointerException.class, null);
  }

  @Test
  public void checkingHigher() {
    assertThat(treeSet.higher(4), equalTo(5));
    assertThat(treeSet.higher(0), equalTo(1));
    assertThat(treeSet.higher(5), nullValue());
    failToTriggerMethod(null, NullPointerException.class, treeSet::higher);
    failToTriggerMethod(1, NullPointerException.class, null);
  }

  private void failToTriggerMethod(
      Integer number, Class<? extends Exception> aClass, ConsumerThrowable<Integer> consumer) {
    try {
      consumer.accept(number);
      fail(FAIL_MESSAGE);
    } catch (Exception ex) {
      assertThat(ex, instanceOf(aClass));
    }
  }

  @Test
  public void checkingPollFirst() {
    assertThat(treeSet.pollFirst(), equalTo(1));
    assertThat((new TreeSet<>()).pollFirst(), nullValue());
  }

  @Test
  public void checkingPollLast() {
    assertThat(treeSet.pollLast(), equalTo(5));
    assertThat((new TreeSet<>()).pollFirst(), nullValue());
  }

  @Test
  public void checkingHead() {
    SortedSet<Integer> headSet =
        sortedSet.headSet(3); // returns a set with elements that are less to parameter supplied.
    assertThat(headSet.size(), equalTo(2));
    assertThat(headSet, equalTo(Set.of(1, 2)));
    failToTriggerMethod(null, NullPointerException.class, sortedSet::headSet);
    failToTriggerMethod(10, NullPointerException.class, null);
  }

  @Test
  public void checkTail() {
    SortedSet<Integer> tailSet =
        sortedSet.tailSet(
            3); // returns a set with elements that are greter or equal to parameter supplied.
    assertThat(tailSet.size(), equalTo(3));
    assertThat(tailSet, equalTo(Set.of(3, 4, 5)));
    failToTriggerMethod(null, NullPointerException.class, sortedSet::tailSet);
    failToTriggerMethod(10, NullPointerException.class, null);
  }

  @Test
  public void checkingSubSet() {
    SortedSet<Integer> subSet = sortedSet.subSet(1, 3);
    assertThat(subSet.size(), equalTo(2));
    assertThat(subSet, equalTo(Set.of(1, 2)));
		assertThat(sortedSet.subSet(3, 3), empty());
		failtToGetSubSet(sortedSet, 3, 1, IllegalArgumentException.class);
		failtToGetSubSet(null, 3, 1, NullPointerException.class);
  }
	
	private void failtToGetSubSet( SortedSet<Integer> sortedSetItems, int fromItem, int toIndex, Class<? extends Exception> triggerException) {
		try {
			sortedSetItems.subSet(fromItem, toIndex);
			fail(FAIL_MESSAGE);
		}catch(Exception ex){
			assertThat( ex, instanceOf( triggerException ));
		}
	}
	
}
