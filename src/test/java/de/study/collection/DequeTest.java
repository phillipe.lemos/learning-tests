/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.study.collection;


import static de.study.Message.FAIL_MESSAGE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.fail;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.function.Function;
import org.junit.Before;
import org.junit.Test;

/** @author PLemos */
public class DequeTest {

  private Deque<Integer> deque;

	@Before
  public void setUp() {
    deque = new ArrayDeque<>();
  }

  @Test
  public void checkingAddFirst() {
    deque.addFirst(1);
    deque.addFirst(2);
    comparingDequeContentConvertToArrayToArray(new Integer[] {2, 1});
  }

  @Test
  public void checkingAddLast() {
    deque.addLast(1);
    deque.addLast(2);
    comparingDequeContentConvertToArrayToArray(new Integer[] {1, 2});
  }

  private void comparingDequeContentConvertToArrayToArray(Integer[] results) {
    assertThat(deque.isEmpty(), equalTo(Boolean.FALSE));
    Integer[] numbers = deque.toArray(new Integer[deque.size()]);
    assertThat(Arrays.equals(numbers, results), equalTo(Boolean.TRUE));
  }

  /**
   * The method getFirst does not move the cursor and does not remove the element. It is equivalent
   * to element().
   */
  @Test
  public void checkingGetFirst() {
    deque.addFirst(3);
    deque.addFirst(5);
    assertThat(deque.getFirst(), equalTo(5));
    assertThat(deque.getFirst(), equalTo(5));
    failToRetrieveAnElementAndThrowAnException(
        new ArrayDeque<>(), Deque<Integer>::getFirst, NoSuchElementException.class);
    failToRetrieveAnElementAndThrowAnException(
        new LinkedList<>(), Deque<Integer>::getFirst, NoSuchElementException.class);
  }

  @Test
  public void checkingGetLast() {
    deque.addLast(3);
    deque.addLast(5);
    assertThat(deque.getLast(), equalTo(5));
    assertThat(deque.getLast(), equalTo(5));
    failToRetrieveAnElementAndThrowAnException(
        new ArrayDeque<>(), Deque<Integer>::getLast, NoSuchElementException.class);
    failToRetrieveAnElementAndThrowAnException(
        new LinkedList<>(), Deque<Integer>::getLast, NoSuchElementException.class);
  }

  private void failToRetrieveAnElementAndThrowAnException(
      Deque<Integer> emptyDeque,
      Function<Deque, Integer> func,
      Class<? extends Exception> exceptionClass) {
    try {
      func.apply(emptyDeque);
      fail(FAIL_MESSAGE);
    } catch (Exception ex) {
      assertThat(ex, instanceOf(exceptionClass));
    }
  }

  @Test
  public void checkingPeekFirst() {
    deque.addFirst(4);
    deque.addFirst(3);
    assertThat(deque.peekFirst(), equalTo(3));
    assertThat(deque.peekFirst(), equalTo(3));
    gettingElement(Deque<Integer>::peekFirst, new ArrayDeque<>());
    gettingElement(Deque<Integer>::peekFirst, new LinkedList<>());
  }

  private void gettingElement(Function<Deque<Integer>, Integer> func, Deque<Integer> emptDeque) {
    assertThat(func.apply(emptDeque), nullValue());
  }

  @Test
  public void checkingPeekLast() {
    deque.addLast(3);
    deque.addLast(4);
    assertThat(deque.peekLast(), equalTo(4));
    assertThat(deque.peekLast(), equalTo(4));
    gettingElement(Deque<Integer>::peekLast, new ArrayDeque<>());
    gettingElement(Deque<Integer>::peekLast, new LinkedList<>());
  }

  @Test
  public void checkingOfferFirst() {
    deque.offerFirst(10);
    deque.offerFirst(8);
    comparingDequeContentConvertToArrayToArray(new Integer[] {8, 10});
  }

  @Test
  public void checkingOfferLast() {
    deque.offerLast(10);
    deque.offerLast(8);
    comparingDequeContentConvertToArrayToArray(new Integer[] {10, 8});
  }

  @Test
  public void checkingRemoveFirst() {
    deque.addFirst(10);
    deque.addFirst(8);
    assertThat(deque.removeFirst(), equalTo(8));
    assertThat(deque.removeFirst(), equalTo(10));
    assertThat(deque.isEmpty(), equalTo(Boolean.TRUE));
    failToRetrieveAnElementAndThrowAnException(
        new ArrayDeque<>(), Deque<Integer>::removeFirst, NoSuchElementException.class);
    failToRetrieveAnElementAndThrowAnException(
        new LinkedList<>(), Deque<Integer>::removeFirst, NoSuchElementException.class);
  }

  @Test
  public void checkingRemoveLast() {
    deque.addFirst(10);
    deque.addFirst(8);
    assertThat(deque.removeLast(), equalTo(10));
    assertThat(deque.removeFirst(), equalTo(8));
    assertThat(deque.isEmpty(), equalTo(Boolean.TRUE));
    failToRetrieveAnElementAndThrowAnException(
        new ArrayDeque<>(), Deque<Integer>::removeLast, NoSuchElementException.class);
    failToRetrieveAnElementAndThrowAnException(
        new LinkedList<>(), Deque<Integer>::removeLast, NoSuchElementException.class);
  }

  @Test
  public void checkingRemoveFirstOcurrence() {
    deque.add(10);
    deque.add(8);
    deque.add(10);
    assertThat(deque.removeFirstOccurrence(10), equalTo(Boolean.TRUE));
    assertThat(deque.removeFirstOccurrence(12), equalTo(Boolean.FALSE));
    comparingDequeContentConvertToArrayToArray(new Integer[] {8, 10});
		failToRetrieveAnElementAndThrowAnException( null, Deque<Integer>::removeFirst, NullPointerException.class  );
  }

	@Test
  public void checkingRemoveLastOcurrence() {
    deque.add(10);
    deque.add(8);
    deque.add(10);
    assertThat(deque.removeLastOccurrence(10), equalTo(Boolean.TRUE));
    assertThat(deque.removeLastOccurrence(12), equalTo(Boolean.FALSE));
    comparingDequeContentConvertToArrayToArray(new Integer[] {10, 8});
		failToRetrieveAnElementAndThrowAnException( null, Deque<Integer>::removeLast, NullPointerException.class  );
	}
}