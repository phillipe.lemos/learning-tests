/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study.collection;

import static de.study.Message.FAIL_MESSAGE;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;
import java.util.function.Predicate;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.fail;
import org.junit.Test;

/** @author PLemos */
public class CollectionsTest {
	
	private final List<Integer> initialList;
	
	private final List<Integer> immutableList;
	
	private final Set<Integer> immutableSet;
	
	public CollectionsTest(){
		initialList = Arrays.asList(1,2,3,4,5);
		immutableList = List.of(1,2,3,4,5);
		immutableSet = Set.of(1,2,3,4,5);
	}
	
	@Test
	public void checkingAddFromListMethod(){
		addTestFromListAndQueue(new ArrayList<>());
		addTestFromListAndQueue(new LinkedList<>());
		addTestFromListAndQueue(new Vector<>());
	}
	
	@Test
	public void checkingMethodAddFromQueue(){
		addTestFromListAndQueue(new ArrayDeque<>());
	}
	
	@Test
	public void checkingMethodAddFromSet(){
		addTestFromSet( new HashSet<>() );
		addTestFromSet( new TreeSet<>() );
	}
	
	private void addTestFromListAndQueue(Collection<Integer> collectionItems){
    assertThat(collectionItems.add(1), equalTo(Boolean.TRUE));
		assertThat(collectionItems.add(1), equalTo(Boolean.TRUE));
		assertThat(collectionItems.add(2), equalTo(Boolean.TRUE));
	}
	
	private void addTestFromSet( Collection<Integer> collectionItems ){
		assertThat(collectionItems.add(1), equalTo(Boolean.TRUE));
		assertThat(collectionItems.add(1), equalTo(Boolean.FALSE));
		assertThat(collectionItems.add(1), equalTo(Boolean.FALSE));
		assertThat(collectionItems.add(2), equalTo(Boolean.TRUE));
	}
	
	@Test
	public void checkingClearMethod(){
    cleanningCollection(new ArrayList<>(initialList)); 
		cleanningCollection(new HashSet<>(initialList)); 
		cleanningCollection(new LinkedList<>(initialList)); 
		cleanningCollection(new TreeSet<>(initialList)); 
		cleanningCollection(new Vector<>(initialList)); 
		cleanningCollection(new ArrayDeque<>(initialList)); 
		tryingToChangeAnImmutableCollection( initialList );
		tryingToChangeAnImmutableCollection( immutableList );
		tryingToChangeAnImmutableCollection(immutableList);
	}
	
	private void cleanningCollection(Collection<Integer> items ){
		assertThat( items.isEmpty(), equalTo( Boolean.FALSE ));
		items.clear();
		assertThat( items.isEmpty(), equalTo( Boolean.TRUE ));
	}
	
	private void tryingToChangeAnImmutableCollection( Collection<Integer> items ) {
		try{
			items.clear();
			fail("It is expected an exception here due the readonly collection");
		}catch(Exception ex) {
			assertThat( ex, instanceOf( UnsupportedOperationException.class) );
		}
	}
	
	@Test
	public void checkingAddAllMethod(){
		addCollectionIntoCollection( new ArrayList<>(), initialList  );
		addCollectionIntoCollection( new LinkedList<>(), initialList  );
		addCollectionIntoCollection( new HashSet<>(), initialList  );
		addCollectionIntoCollection( new TreeSet<>(), initialList  );
		addCollectionIntoCollection( new ArrayDeque<>(), initialList  );
		addCollectionIntoCollection( new Vector<>(), initialList  );
	}
	
	private void addCollectionIntoCollection( Collection<Integer> targetCollection, Collection<Integer>  sourceCollection ) {
		assertThat( targetCollection.isEmpty(), equalTo(Boolean.TRUE) );
		assertThat( sourceCollection.isEmpty(), equalTo(Boolean.FALSE) );
		assertThat( targetCollection.addAll(sourceCollection), equalTo(Boolean.TRUE) );
	}
	
	@Test
	public void checkingContainsAll() {
		Collection<Integer> mySimpleList = new ArrayList<>(Arrays.asList(1,2,3,4));
		Collection<Integer> mySimpleSet = new HashSet<>(Arrays.asList(1,2,3,4));
    assertThat(mySimpleList.containsAll(mySimpleSet), equalTo( Boolean.TRUE) );
		assertThat(mySimpleSet.containsAll(mySimpleList), equalTo( Boolean.TRUE) );
	}
	
	@Test
	public void checkingIsEmpty() {
    isEmptyCollection(new ArrayList<>(), Boolean.TRUE);
    isEmptyCollection(initialList, Boolean.FALSE);
    isEmptyCollection(immutableList, Boolean.FALSE);
		isEmptyCollection(immutableSet, Boolean.FALSE);
		isEmptyCollection(new HashSet<>(), Boolean.TRUE);
		isEmptyCollection(new TreeSet<>(), Boolean.TRUE);
    isEmptyCollection(new LinkedList<>(), Boolean.TRUE);
		isEmptyCollection(new ArrayDeque<>(), Boolean.TRUE);
	}
	
	private void isEmptyCollection( Collection<Integer>  itemsToCheck, Boolean expectedResult ){
		assertThat(itemsToCheck.isEmpty(), equalTo( expectedResult  ));
	}
	
	@Test
	public void checkingRemove(){
     removeItemFromCollectionAndChecking( new ArrayList<>(initialList) );
		 removeItemFromCollectionAndChecking( new LinkedList<>(initialList) );
		 removeItemFromCollectionAndChecking( new Vector<>(initialList) );
		 removeItemFromCollectionAndChecking( new HashSet<>(initialList) );
		 removeItemFromCollectionAndChecking( new TreeSet<>(initialList) );
		 removeItemFromCollectionAndChecking( new ArrayDeque<>(initialList) );
		 removeFailDueImmutableCollections(immutableList);
		 removeFailDueImmutableCollections(immutableSet);
		 removeFailDueImmutableCollections(initialList);
	}
	
	private void removeItemFromCollectionAndChecking( Collection<Integer> items  ){
		assertThat( items.size(), equalTo(5) );
		assertThat(items.remove(Integer.valueOf(1)), equalTo(Boolean.TRUE));
		assertThat(items.remove(Integer.valueOf(1)), equalTo(Boolean.FALSE));
		assertThat( items.size(), equalTo(4) );
	}
	
	private void removeFailDueImmutableCollections(Collection<Integer> items) {
		try{
			items.remove( Integer.valueOf(1));
			fail(FAIL_MESSAGE);
		} catch(Exception ep){
			assertThat( ep, instanceOf(UnsupportedOperationException.class));
		}
	}
	
	@Test
	public void checkingRemoveAllList(){
			var subCollection = Arrays.asList(1,2,3,4);
			removeAllSubCollectionFromCollection( new ArrayList<>(initialList),  subCollection  );
			removeAllSubCollectionFromCollection( new LinkedList<>(initialList),  subCollection  );
			removeAllSubCollectionFromCollection( new HashSet<>(initialList),  subCollection  );
			removeAllSubCollectionFromCollection( new TreeSet<>(initialList),  subCollection  );
			removeAllSubCollectionFromCollection( new ArrayDeque<>(initialList),  subCollection  );
			removeAllSubCollectionFromCollection( new Vector<>(initialList),  subCollection  );
			failToRemoveAllElementsInImmutableCollection( immutableList, subCollection );
			failToRemoveAllElementsInImmutableCollection( immutableSet, subCollection );
			
	}

	private void removeAllSubCollectionFromCollection(Collection<Integer> sourceCollection, Collection<Integer> subCollection) {
		assertThat( sourceCollection.removeAll(subCollection), equalTo( Boolean.TRUE ) );
		assertThat( sourceCollection.removeAll(subCollection), equalTo( Boolean.FALSE ) );
	  assertThat( sourceCollection.size(), equalTo(1));
	}
	
	
	private void failToRemoveAllElementsInImmutableCollection(Collection<Integer> sourceCollection, Collection<Integer> subCollection){
		try{
			sourceCollection.removeAll(subCollection);
			fail(FAIL_MESSAGE);
		}catch(Exception ex) {
			assertThat( ex, instanceOf( UnsupportedOperationException.class ));
		}
	}
	
	@Test
	public void checkingRemoveIf(){
		Predicate<Integer> commonPredicate = i -> i > 0;
		removeFromCollection( new ArrayList<>(initialList), commonPredicate );
		removeFromCollection( new LinkedList<>(initialList), commonPredicate);
		removeFromCollection( new HashSet<>(initialList), commonPredicate);
		removeFromCollection( new TreeSet<>(initialList), commonPredicate);
		removeFromCollection( new ArrayDeque<>(initialList), commonPredicate);
		removeFromCollection( new Vector<>(initialList), commonPredicate);
		failToRemoveByPredicateDueImmutableSourceCollection( immutableList, commonPredicate );
		failToRemoveByPredicateDueImmutableSourceCollection( immutableSet, commonPredicate );
	}
	
	private void removeFromCollection(Collection<Integer> sourceItems, Predicate<Integer> predicate ){
		assertThat( sourceItems.isEmpty(), equalTo( Boolean.FALSE));
		assertThat( sourceItems.removeIf(predicate), equalTo( Boolean.TRUE));
		assertThat( sourceItems.removeIf(predicate), equalTo( Boolean.FALSE));
		assertThat( sourceItems.isEmpty(), equalTo( Boolean.TRUE));
	}
	
	private void failToRemoveByPredicateDueImmutableSourceCollection(Collection<Integer> sourceItems, Predicate<Integer> predicate ){
		try {
			sourceItems.removeIf(predicate);
			fail(FAIL_MESSAGE);
		} catch(Exception ex) {
			assertThat( ex, instanceOf( UnsupportedOperationException.class ));
		}
	}
	
	@Test
	public void checkingRetainAll() {
		var elementsToBeRetained = Arrays.asList(1,2,3,4);
		retainElementsFromCollection( new ArrayList<>(initialList), elementsToBeRetained );
		retainElementsFromCollection( new LinkedList<>(initialList), elementsToBeRetained);
		retainElementsFromCollection( new HashSet<>(initialList), elementsToBeRetained);
		retainElementsFromCollection( new TreeSet<>(initialList), elementsToBeRetained);
		retainElementsFromCollection( new ArrayDeque<>(initialList), elementsToBeRetained);
		retainElementsFromCollection( new Vector<>(initialList), elementsToBeRetained);
		failToRetainElementsFromImmutableCollection( immutableList, elementsToBeRetained );
		failToRetainElementsFromImmutableCollection( immutableSet, elementsToBeRetained );
		
	}
	
	private void retainElementsFromCollection( Collection<Integer> sourceItems, Collection<Integer>  elementsToBeRetained ) {
		assertThat(  sourceItems.retainAll(elementsToBeRetained), equalTo(Boolean.TRUE));
		assertThat(  sourceItems.retainAll(elementsToBeRetained), equalTo(Boolean.FALSE));
		assertThat( sourceItems.size(), equalTo( 4 ));
	}
	
	
	private void failToRetainElementsFromImmutableCollection(Collection<Integer> sourceItems, Collection<Integer>  elementsToBeRetained){
		try {
      sourceItems.retainAll(elementsToBeRetained);
			fail(FAIL_MESSAGE);
		} catch(Exception ex) {
			assertThat( ex, instanceOf( UnsupportedOperationException.class ));
		}
	}
	
	@Test
	public void checkingToArray() {
		Integer[] expectedArray = {1,2,3,4,5};
    toArrayFromCollections( new ArrayList<>(initialList), expectedArray );
		toArrayFromCollections( new LinkedList<>(initialList), expectedArray );
		toArrayFromCollections( new HashSet<>(initialList), expectedArray );
		toArrayFromCollections( new TreeSet<>(initialList), expectedArray );
		toArrayFromCollections( new Vector<>(initialList), expectedArray );
		toArrayFromCollections( new ArrayDeque<>(initialList), expectedArray );
	}
	
	
	private void toArrayFromCollections(Collection<Integer> sourceItems, Integer[] expectedArray ) {
		Integer[] resultArray = initialList.toArray(new Integer[sourceItems.size()]);
    assertThat( Arrays.equals(expectedArray, resultArray), equalTo( Boolean.TRUE ));
	}
}