/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.study.collection;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import java.util.Arrays;
import org.junit.Test;


/** @author PLemos */
public class ArraysTest {

  @Test(expected = ArrayStoreException.class)
  public void storingWrongKindOfDataIntoArray() {
    Integer[] numbers = {Integer.valueOf(10), Integer.valueOf(12)};
		Object[] objects = numbers;
		objects[0] = "forty two";
	}
	
  @Test
  public void understandingArraysCompare(){
	  int[] vet1 = {1,2,3,4};
	  int[] vet2 = {1,2,3,4};
	  assertThat( Arrays.compare(vet1, vet2), equalTo(0));
	  int[] vet3 = {1,2,3};
	  assertThat( Arrays.compare(vet1, vet3), greaterThan(0));
	  assertThat( Arrays.compare(vet3, vet1), lessThan(0));
	  assertThat( Arrays.compare(vet3, null), greaterThan(0));
	  assertThat( Arrays.compare(null, vet1), lessThan(0));
  }
  
  /**
   * BinarySearch requires a sorted array and the sort order should match with the method option. This means 
   * that with you not supply a comparator, it is expected a ascending order.
   * For elements not present in the array, the rule is  :
   * Take the negative index of where it would be inserted and subtract 1.
   */
  @Test
  public void understandingBinarySearch() {
	  int[] vet1 = {1,2,3,4};
	  assertThat(Arrays.binarySearch(vet1, 3), equalTo(2));
	  assertThat(Arrays.binarySearch(vet1, 100), equalTo(-5));
	  assertThat(Arrays.binarySearch(vet1, 0), equalTo(-1));
  }
  

}
